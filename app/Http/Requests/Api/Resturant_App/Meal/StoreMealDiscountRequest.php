<?php

namespace App\Http\Requests\Api\Resturant_App\Meal;

use Illuminate\Foundation\Http\FormRequest;

class StoreMealDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meal_id'                         => 'required|integer',
            'discount_type'                   => 'required|integer',
            'discount_value'                  => 'required|string',
            'discount_start_date'             => 'required|date',
            'discount_end_date'               => 'required|date',
        ];
    }
}

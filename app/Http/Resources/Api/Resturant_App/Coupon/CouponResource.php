<?php

namespace App\Http\Resources\Api\Resturant_App\Coupon;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'resturant_id'      => $this->resturant_id,
            'code'              => $this->code,
            'minimum_price'     => $this->minimum_price,
            'discount_amount'   => $this->discount_amount,
            'discount_type'     => $this->discount_type,
            'start_date'        => $this->start_date,
            'end_date'          => $this->end_date,
        ];

    }
}

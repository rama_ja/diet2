<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResturantModification extends Model implements  TranslatableContract
{
    use HasFactory, Translatable;
    protected $fillable = ['resturant_id','email', 'mobile', 'city_id', 'latitude', 'longitude', 'logo', 'main_image', 'commercial_record', 'municipal_licence', 'language'];
    public $translatedAttributes = ['name', 'description'];

    ##---------- Relationships ----------##

    /**
     * Get the images for the resturant.
     */
    public function images()
    {
        return $this->hasMany(ResturantImageModification::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

}

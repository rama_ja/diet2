<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;
    protected $fillable = ['client_id','representative_id','representative_evaluation','order_id','order_evaluation','resturant_id','resturant_evaluation','client_experience'];
    ##---------- Relationships ----------##

    public function resturant()
    {
        return $this->belongsTo(Resturant::class);
    }
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function representative()
    {
        return $this->belongsTo(Representative::class);
    }
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}

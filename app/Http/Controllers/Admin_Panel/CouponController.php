<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Coupon\StoreCouponRequest;
use App\Http\Resources\Api\Resturant_App\Coupon\CouponResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Coupon;
use App\Models\Resturant;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    use  ApiResponseTrait;

    public function index(Request $request)
    {
        $admin=auth('admin')->user();
        $coupons = Coupon::orderBy('created_at', 'DESC')->paginate(15);
        return view('admin.coupons.index',['coupons'=>$coupons,'admin'=>$admin]);
    }
    public function search(Request $request)
    {
        if($request->ajax())
        {
            $output="";

            $scoupon=$request->search;
            $coupons=Coupon::whereHas('resturant', function($q) use($scoupon){
                $q->whereHas('translations', function($qq) use($scoupon){
                    $qq->where('name','LIKE','%'.$scoupon."%")
                        ->where('locale','ar');
                });
            })->paginate(15);

            if($coupons)
            {$counter=1;
            $output.='<table id="myTable1" class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center" scope="col"></th>
                    <th scope="col">كود الحسم</th>
                    <th scope="col">المطعم التابع له </th>
                    <th scope="col">تاريخ الإضافة</th>
                    <th class="text-center" scope="col"></th>
                </tr>
                </thead>
                <tbody>';
                foreach ($coupons as $key => $coupon) {
                    $output.='<tr>'.
                        '      <td>
                            <p class="text-center">'.$counter.'</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <div class="media">
                              
                                <div class="media-body align-self-center">
                                    <h6 class="mb-0">'.$coupon->code.'</h6>
                                 
                                </div>
                            </div>
                        </td>
                        <td>
                            <p class="mb-0">'.$coupon->resturant->translate('ar')->name.'</p>
                            <span class="text-success"></span>
                        </td>
                         <td>
                            <p class="mb-0">'.$coupon->created_at->toDateString().'</p>
                            <span class="text-success"></span>
                        </td>
                        <td class="text-center">
                            <div class="action-btns">
                                <a href="'.route('admin_panel.coupons.show',$coupon->id).'" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </a>                   
                                <a href="/admin_panel/coupon_del/'.$coupon->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </a>
                            </div>
                        </td>'.
                        '</tr>';
                    $counter++;
                }
                $output.='  <tr>

                    <td colspan="4">

                        <a class="btn btn-primary mb-2 me-4" href="{{route(\'admin_panel.coupons.create\')}}">إضافة كود حسم </a>

                    </td>

                </tr>
                </tbody>

            </table>
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                    </div>
                </div>
                <div class="col-sm-12 col-md-7">

                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                  

                    </div>
                </div>
            </div>
             <script>

            addPagerToTables(\'#myTable1\', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector(\'tr\').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector(\'.nav\')
                        .insertAdjacentHTML(
                            \'beforeend\',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll(\'.nav a\'))
                    navA.addEventListener(
                        \'click\',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll(\'.nav a\');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add(\'active\');
                    else
                        navAs[nix].classList.remove(\'active\');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? \'table-row\'
                                : \'none\';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll(\'tbody tr\');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll(\'td\').length > 0);
            }
        </script>';
                return Response($output);
            }
        }
    }

    public function show($id)
    {
        $admin=auth('admin')->user();
        $coupon=Coupon::find($id);
        return view('admin.coupons.item',['admin'=>$admin,'coupon'=>$coupon]);

    }
    public function create()
    {
        $admin=auth('admin')->user();
        $resturants=Resturant::orderBy('created_at', 'DESC')->get();
        return view('admin.coupons.create',['admin'=>$admin,'resturants'=>$resturants]);

    }
    public function store(StoreCouponRequest $request)
    {

        $coupon = Coupon::create([
            'resturant_id'      => $request->resturant_id,
            'code'              => $request->code,
            'minimum_price'     => $request->minimum_price,
            'discount_amount'   => $request->discount_amount,
            'discount_type'     => $request->discount_type,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date
        ]);

        return redirect(route('admin_panel.coupons.index'));

    }

    public function destroy($id)
    {
        $coupon=Coupon::find($id);
        $coupon->delete();
        return redirect()->back();
    }

}



<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResturantImageModification extends Model
{
    use HasFactory;
    protected $fillable = ['resturant_modification_id', 'image'];
    ##---------- Relationships ----------##

    /**
     * Get the resturant that owns the image.
     */
    public function resturant()
    {
        return $this->belongsTo(ResturantModification::class);
    }


}

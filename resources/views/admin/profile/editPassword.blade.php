<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.topHeader')
<body class="form">

<!-- BEGIN LOADER -->
<div id="load_screen"> <div class="loader"> <div class="loader-content">
            <div class="spinner-grow align-self-center"></div>
        </div></div></div>
<!--  END LOADER -->

<div class="auth-container d-flex h-100">

    <div class="container mx-auto align-self-center">

        <div class="row">

            <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-8 col-12 d-flex flex-column align-self-center mx-auto">
                <div class="card mt-3 mb-3">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12 mb-3">

                                <div class="media mb-4">

                                    <div class="avatar avatar-lg me-3">
                                        <img alt="avatar" src="/{{$admin->personal_image}}" class="rounded-circle">
                                    </div>

                                    <div class="media-body align-self-center">

                                        <h3 class="mb-0">{{$admin->name}}</h3>
{{--                                        <p class="mb-0">Enter your password to unlock your ID</p>--}}

                                    </div>

                                </div>

                            </div>
                            <form method="post" action="/admin_panel/update_password">
                                @csrf
                                @if ($errors->any())

                                    <div class="alert alert-danger">

                                        <ul style="list-style: none;margin:0">

                                            @foreach ($errors->all() as $error)

                                                <li>{{ $error }}</li>

                                            @endforeach

                                        </ul>

                                    </div>

                                @endif
                                <div class="col-12">
                                    <div class="mb-4">
                                        <label class="form-label"> كلمة المرور القديمة </label>
                                        <input type="password" class="form-control" name="old_password">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-4">
                                        <label class="form-label"> كلمة المرور الجديدة </label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-4">
                                        <p>يجب أن تكون كلمة المرور مكونة من ثماني محارف على الأقل و تحوي رمز و رقم و محرف صفير و محرف كبير </p>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-4">
                                        <label class="form-label"> تأكيد كلمة المرور الجديدة</label>
                                        <input type="password" class="form-control" name="password_confirmation">
                                    </div>
                                </div>
{{--                                <div class="col-12">--}}
{{--                                    <div class="mb-4">--}}
{{--                                        @if($errors->any())--}}
{{--                                            <h4>{{$errors->first()}}</h4>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-12">
                                    <div class="mb-4">
                                        <button class="btn btn-secondary w-100">تعديل</button>
                                    </div>
                                </div>
                            </form>


                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@include('admin.layouts.scripts')
</body>
</html>

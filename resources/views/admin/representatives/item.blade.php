@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">المندوبين </li>
                                    <li class="breadcrumb-item active"> عرض المندوب </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class=""> معلومات المندوب</h3>

                        </div>
{{--                        <div class=" user-info " style="padding-right:  20% ">--}}

{{--                                <img src="/{{$representative->personal_image}}" alt="avatar">--}}
{{--                        --}}{{--                            <p>{{$advisor->name}}</p>--}}
{{--                        </div>--}}
                        <div class="" style="padding:0px 2% ; font-weight: bold;">
                            <div class="container">
                                <div class="row">
                                    <div class="row">

                                            <div class="col-md-6 ">
{{--                                                <label for="inputAddress" class="form-label"> الصورة الشخصية </label><br>--}}
                                                <img alt="avatar" src="/{{$representative->personal_image}}"  width="200" height="200"/>
                                                <br><br>
                                            </div>

                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail4" class="form-label">البريد الالكتروني : </label>
                                                @if($representative->email)
                                                    <span>{{$representative->email}}</span>
                                                @else
                                                    <span>لا يوجد</span>
                                                @endif
                                            </div>

                                            <div class="col-md-6">
                                                <label for="inputEmail4" class="form-label">رقم الجوال : </label>
                                                <span>{{$representative->mobile}}</span>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="inputEmail4" class="form-label">الاسم  : </label>
                                                <span>{{$representative->name}}</span>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="inputAddress" class="form-label"> المطعم التابع له : </label>
                                                @if($representative->resturant)
                                                    <span>{{$representative->resturant->translate('ar')->name}}</span>
                                                @else
                                                   <span> لا يوجد</span>
                                                @endif
                                            </div>



                                            <div class="row col-md-6">
                                                <label for="inputEmail4" class="form-label"> تفاصيل الحافلة </label>
                                                <div class="col-md-4">
                                                    <label for="inputState" class="form-label">الماركة</label>
                                                    <p>{{$representative->vehicle_brand}}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="inputState" class="form-label"> النوع </label>
                                                    <p>{{$representative->type}}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="inputState" class="form-label"> الموديل </label>
                                                    <p>{{$representative->vehicle_model}}</p>
                                                </div>

                                            </div>
                                        <div class="col-md-6">
                                            <label for="inputEmail4" class="form-label">نوع الحافلة : </label>
                                            <span>{{$representative->vehicle_type}}</span>
                                        </div>
                                            <div class="col-md-6">
                                                <label  for="inputEmail4" class="form-label"> رقم الحافلة : </label>
                                                @if($representative->vehicle_number)
                                                    <span>{{$representative->vehicle_number}}</span>
                                                @else
                                                    لا يوجد
                                                @endif
                                            </div>
                                            {{--                <!--     <div  class="col-md-6">--}}
                                            {{--                    <label for="inputEmail4" class="form-label">الوصف باللغة الأجنبية</label>--}}
                                            {{--                    <input type="text" class="form-control"  value="{{$resturant->translate('en')->description}}">--}}
                                            {{--                </div>--}}
                                            {{--          -->--}}
                                            <div class="col-md-6">
                                                <label for="inputState" class="form-label">المدينة : </label>
                                                <span>{{$representative->city->translate('ar')->name}}</span>
                                            </div>



                                            <div class="col-md-6">
                                                <label for="inputState" class="form-label">الحالة : </label>
                                                @if($representative->status ==1)
                                                    <span> مقبول </span>
                                                @elseif($representative->status ==2)
                                                    <span> غير مقبول </span>
                                                @endif
                                            </div>
                                        <div class="col-md-6">

                                            </div>

                                        <div class="col-md-6 ">
                                            <label for="inputAddress" class="form-label"> الاستمارة </label><br>
                                            <img alt="avatar" src="/{{$representative->form_image}}"  width="300" height="200"/>
                                            <br><br>
                                        </div>

                                        <div class="col-md-6 ">
                                            <label for="inputAddress" class="form-label"> صورة الشهادة  </label><br>
                                            <img alt="avatar" src="/{{$representative->license_image}}"  width="200" height="200"/>
                                            <br><br>
                                        </div>
                                    <!--
        {{--                <div class="col-md-6">--}}
                                    {{--                    <label for="inputEmail4" class="form-label">الاسم باللغة الأجنبية</label>--}}
                                    {{--                    <input type="text" class="form-control"  value="{{$resturant->translate('en')->name}}">--}}
                                    {{--                </div>--}}
                                        -->
                                        <div class="col-12">
                                            <div class="row">
                                                <div id="splider_MultipleSlider" class="col-lg-12 col-md-12 layout-spacing">
                                                    <div class="statbox widget box box-shadow">
                                                        <div class="widget-header">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                    <h4>صور الحافلة </h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="widget-content widget-content-area">
                                                            <div class="position-relative">
                                                                <div class="container" style="max-width: 700px">
                                                                    <div class="splide-multiple">
                                                                        <div class="splide__track">
                                                                            <ul class="splide__list">
                                                                                @foreach($representative->images as $img)
                                                                                    <li class="splide__slide">
                                                                                        <img alt="slider-image" class="img-fluid" src="/{{$img->image}}" width="100" height="100">
                                                                                    </li>
                                                                                @endforeach

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        <div class="container">--}}

{{--            <div class="col-md-6">--}}
{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputAddress" class="form-label"> الصورة الشخصية </label>--}}
{{--                        <img alt="avatar" src="/{{$representative->personal_image}}"  width="200" height="200"/>--}}
{{--                        <br><br>--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputEmail4" class="form-label">البريد الالكتروني</label>--}}
{{--                        @if($representative->email)--}}
{{--                             <p>{{$representative->email}}</p>--}}
{{--                        @else--}}
{{--                        لا يوجد--}}
{{--                        @endif--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputEmail4" class="form-label">رقم الجوال</label>--}}
{{--                        <p>{{$representative->mobile}}</p>--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputEmail4" class="form-label">الاسم </label>--}}
{{--                        <p>{{$representative->name}}</p>--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputAddress" class="form-label"> المطعم التابع له </label>--}}
{{--                        @if($representative->resturant)--}}
{{--                            <p>{{$representative->resturant->translate('ar')->name}}</p>--}}
{{--                        @else--}}
{{--                            لا يوجد--}}
{{--                        @endif--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputEmail4" class="form-label">نوع الحافلة </label>--}}
{{--                        <p>{{$representative->vehicle_type}}</p>--}}
{{--                    </div>--}}

{{--                    <div class="row col-md-6">--}}
{{--                        <label for="inputEmail4" class="form-label"> تفاصيل الحافلة </label>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <label for="inputState" class="form-label">الماركة</label>--}}
{{--                            <p>{{$representative->vehicle_brand}}</p>--}}
{{--                            <p></p>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <label for="inputState" class="form-label"> النوع </label>--}}
{{--                            <p>{{$representative->type}}</p>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <label for="inputState" class="form-label"> الموديل </label>--}}
{{--                            <p>{{$representative->vehicle_model}}</p>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="col-12">--}}
{{--                        <div class="row">--}}
{{--                            <div id="splider_MultipleSlider" class="col-lg-12 col-md-12 layout-spacing">--}}
{{--                                <div class="statbox widget box box-shadow">--}}
{{--                                    <div class="widget-header">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">--}}
{{--                                                <h4>صور الحافلة </h4>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="widget-content widget-content-area">--}}
{{--                                        <div class="position-relative">--}}
{{--                                            <div class="container" style="max-width: 700px">--}}
{{--                                                <div class="splide-multiple">--}}
{{--                                                    <div class="splide__track">--}}
{{--                                                        <ul class="splide__list">--}}
{{--                                                            @foreach($representative->images as $img)--}}
{{--                                                                <li class="splide__slide">--}}
{{--                                                                    <img alt="slider-image" class="img-fluid" src="/{{$img->image}}" width="100" height="100">--}}
{{--                                                                </li>--}}
{{--                                                            @endforeach--}}

{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}


{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            <div class="col-md-12">--}}
{{--                <div class="col-md-6">--}}
{{--                    <label for="inputAddress" class="form-label"> صورة الشهادة  </label>--}}
{{--                    <img alt="avatar" src="/{{$representative->license_image}}"  width="200" height="200"/>--}}
{{--                    <br><br>--}}
{{--                </div>--}}
{{--                <!----}}
{{--                <div class="col-md-6">--}}
{{--                    <label for="inputEmail4" class="form-label">الاسم باللغة الأجنبية</label>--}}
{{--                    <input type="text" class="form-control"  value="{{$resturant->translate('en')->name}}">--}}
{{--                </div>--}}
{{--            -->--}}
{{--                    <div class="col-md-6">--}}
{{--                        <label  for="inputEmail4" class="form-label"> رقم الحافلة </label>--}}
{{--                        @if($representative->vehicle_number)--}}
{{--                            <p>{{$representative->vehicle_number}}</p>--}}
{{--                        @else--}}
{{--                         لا يوجد--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                <!--     <div  class="col-md-6">--}}
{{--                    <label for="inputEmail4" class="form-label">الوصف باللغة الأجنبية</label>--}}
{{--                    <input type="text" class="form-control"  value="{{$resturant->translate('en')->description}}">--}}
{{--                </div>--}}
{{--          -->--}}
{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputState" class="form-label">المدينة</label>--}}
{{--                        <p>{{$representative->city->translate('ar')->name}}</p>--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputAddress" class="form-label"> الاستمارة </label>--}}
{{--                        <img alt="avatar" src="/{{$representative->form_image}}"  width="300" height="200"/>--}}
{{--                        <br><br>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6">--}}
{{--                        <label for="inputState" class="form-label">الحالة</label>--}}
{{--                        @if($representative->status ==1)--}}
{{--                            <p> مقبول </p>--}}
{{--                        @elseif($representative->status ==2)--}}
{{--                            <p> غير مقبول </p>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--        </div>--}}
        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection

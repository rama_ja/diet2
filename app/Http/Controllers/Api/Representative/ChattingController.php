<?php

namespace App\Http\Controllers\Api\Representative;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Representative_App\MessageResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Chatting;
use App\Models\Order;
use Illuminate\Http\Request;

class ChattingController extends Controller
{
    use  ApiResponseTrait;
    public function index($id)
    {
        $order=Order::find($id);
        $message=Chatting::where('order_id',$order->id)->orderBy('created_at', 'DESC')->paginate(20);


        return $this->apiResponse(MessageResource::collection($message), 'Chatting for this order ', 200);
    }
    public function sendMessage(Request $request,$id){
        $representative = $request->user('representative_api');
        $order=Order::find($id);
        $message=Chatting::create([
            'order_id'              => $order->id,
            'sender'                => $request->sender,
            'message'               => $request->message
        ]);

        return $this->apiResponse(new MessageResource($message), 'Message has been sent successfully', 200);

    }

}

<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Admin;
use App\Models\City;
use App\Models\Meal;
use App\Models\Package;
use App\Models\Resturant;
use App\Models\ResturantImage;
use App\Models\ResturantImageModification;
use App\Models\ResturantModification;
use App\Models\ResturantTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ResturantController extends Controller
{
    use SaveImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin=auth('admin')->user();
        $resturants=Resturant::where('status', '!=', '0')->orderBy('created_at', 'DESC')->paginate(15, ['*'], 'resturants');
        $join_req=Resturant::where('status','1')->orderBy('created_at', 'DESC')->paginate(15, ['*'], 'join_req');
        $change_req=Resturant::where('status','2')->orderBy('created_at', 'DESC')->paginate(15, ['*'], 'change_req');
        $accepted=Resturant::where('status','3')->orderBy('created_at', 'DESC')->paginate(15, ['*'], 'accepted');
        $rejected=Resturant::where('status','4')->orderBy('created_at', 'DESC')->paginate(15, ['*'], 'rejected');
        return view('admin.resturants.index',['admin'=>$admin,'resturants'=>$resturants,'join_req'=>$join_req,'change_req'=>$change_req,'accepted'=>$accepted,'rejected'=>$rejected,]);
    }
    public function search(Request $request,$id)
    {
        if($request->ajax())
        {
            $output="";
            $search_v=$request->search;
            if($id==0){
                $resturants=Resturant::where('email','LIKE','%'.$search_v."%")
                    ->where('status','!=','0')
                    ->orwhereHas('translations', function($qq) use($search_v){
                        $qq->where('name','LIKE','%'.$search_v."%")
                            ->where('locale','ar');
                    })->where('status','!=','0')
                    ->orderBy('created_at', 'DESC')->get();

            }elseif ($id==1){

                $resturants=Resturant::where('email','LIKE','%'.$search_v."%")
                    ->where('status','1')
                    ->orwhereHas('translations', function($qq) use($search_v){
                        $qq->where('name','LIKE','%'.$search_v."%")
                            ->where('locale','ar');
                    })->where('status','1')
                    ->orderBy('created_at', 'DESC')->get();

            }elseif ($id==2){

                $resturants=Resturant::where('email','LIKE','%'.$search_v."%")
                    ->where('status','2')
                    ->orwhereHas('translations', function($qq) use($search_v){
                        $qq->where('name','LIKE','%'.$search_v."%")
                            ->where('locale','ar');
                    })->where('status','2')
                    ->orderBy('created_at', 'DESC')->get();

            }elseif ($id==3){

                $resturants=Resturant::where('email','LIKE','%'.$search_v."%")
                    ->where('status','3')
                    ->orwhereHas('translations', function($qq) use($search_v){
                        $qq->where('name','LIKE','%'.$search_v."%")
                            ->where('locale','ar');
                    })->where('status','3')
                    ->orderBy('created_at', 'DESC')->get();

            }elseif ($id==4){

                $resturants=Resturant::where('email','LIKE','%'.$search_v."%")
                    ->where('status','4')
                    ->orwhereHas('translations', function($qq) use($search_v){
                        $qq->where('name','LIKE','%'.$search_v."%")
                            ->where('locale','ar');
                    })->where('status','4')
                    ->orderBy('created_at', 'DESC')->paginate(15);

            }
            if($resturants)
            {$counter=1;


                    $output.='
  
           
            <table id="myTable5" class="table table-bordered">
                <thead>
                
                            <tr>
                                <th class="text-center" scope="col"></th>
                                <th scope="col">اسم المطعم</th>
                                <th scope="col">تاريخ الانضمام</th>
                                <th class="text-center" scope="col">الحالة</th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>';
                foreach ($resturants as $key => $resturant) {
                    $output.='  <tr>
                               <td>
                            <p class="text-center">'.$counter.'</p>
                            <span class="text-success"></span>
                        </td>
                       <td>
                            <div class="media">
                                <div class="avatar me-2">
                                    <img alt="avatar" src="/'.$resturant->logo.'" class="rounded-circle" />
                                </div>
                                <div class="media-body align-self-center">
                                    <h6 class="mb-0">'.$resturant->translate('ar')->name.'</h6>
                                    <span>'.$resturant->email.'</span>
                                </div>
                            </div>
                        </td>
                          <td>
                            <p class="mb-0">'.$resturant->created_at->toDateString().'</p>
                            <span class="text-success"></span>
                        </td>
                         <td class="text-center">
                                    <span class="badge badge-light-success">';
                                        if($resturant->status ==1)
                                            $output.= 'بانتظار القبول';
                                        elseif($resturant->status ==2)
                                            $output.= ' بانتظار قبول التعديلات';
                                        elseif($resturant->status ==3)
                                            $output.= ' مقبول';
                                        elseif($resturant->status ==4)
                                            $output.= ' مرفوض';
                                  $output.='   </span>
                                        </td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="'.route('admin_panel.resturants.show',$resturant->id).'" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                </a>
                                                <a href="'.route('admin_panel.resturants.edit',$resturant->id).'" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                </a>
                                                <a href="/admin_panel/resturant_del/'.$resturant->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>';
                                        if($resturant->status =='2' ){
                                            $output.='          <a href="/admin_panel/reject/'.$resturant->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="رفض">
                                                          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                         width="100" height="100"
                                                         viewBox="0 0 50 50">
                                                        <path d="M 25 2 C 12.309534 2 2 12.309534 2 25 C 2 37.690466 12.309534 48 25 48 C 37.690466 48 48 37.690466 48 25 C 48 12.309534 37.690466 2 25 2 z M 25 4 C 36.609534 4 46 13.390466 46 25 C 46 36.609534 36.609534 46 25 46 C 13.390466 46 4 36.609534 4 25 C 4 13.390466 13.390466 4 25 4 z M 32.990234 15.986328 A 1.0001 1.0001 0 0 0 32.292969 16.292969 L 25 23.585938 L 17.707031 16.292969 A 1.0001 1.0001 0 0 0 16.990234 15.990234 A 1.0001 1.0001 0 0 0 16.292969 17.707031 L 23.585938 25 L 16.292969 32.292969 A 1.0001 1.0001 0 1 0 17.707031 33.707031 L 25 26.414062 L 32.292969 33.707031 A 1.0001 1.0001 0 1 0 33.707031 32.292969 L 26.414062 25 L 33.707031 17.707031 A 1.0001 1.0001 0 0 0 32.990234 15.986328 z"></path>
                                                    </svg>
                                                    </a>
                                                    <a href="/admin_panel/accept/'.$resturant->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="قبول">
                                                         <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                         width="100" height="100"
                                                         viewBox="0 0 128 128">
                                                        <path d="M 64 6 C 32 6 6 32 6 64 C 6 96 32 122 64 122 C 96 122 122 96 122 64 C 122 32 96 6 64 6 z M 64 12 C 92.7 12 116 35.3 116 64 C 116 92.7 92.7 116 64 116 C 35.3 116 12 92.7 12 64 C 12 35.3 35.3 12 64 12 z M 85.037109 48.949219 C 84.274609 48.974219 83.500391 49.300391 82.900391 49.900391 L 62 71.599609 L 51.099609 59.900391 C 49.999609 58.700391 48.100391 58.599219 46.900391 59.699219 C 45.700391 60.799219 45.599219 62.700391 46.699219 63.900391 L 59.800781 78 C 60.400781 78.6 61.1 79 62 79 C 62.8 79 63.599219 78.699609 64.199219 78.099609 L 87.199219 54 C 88.299219 52.8 88.299609 50.900781 87.099609 49.800781 C 86.549609 49.200781 85.799609 48.924219 85.037109 48.949219 z"></path>
                                                    </svg>
                                                    </a>
                                                ';
                                        }
                                  $output.='           </div>
                                        </td>
                    </tr>';
                    $counter++;
                }

                $output.='            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">

                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                                

                                </div>
                            </div>
                        </div>
                   ';


                return Response($output);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin=auth('admin')->user();
        $resturant=Resturant::find($id);
        $meals = Meal::where('resturant_id', $id)->get();
        $packages = Package::where('resturant_id',$resturant->id)->get();
        $sum=0;
        foreach ($resturant->rates as $rate){
            $sum+=$rate->evaluation;
        }
        if($resturant->rates()->count()>0){
            $rates_avg=$sum/$resturant->rates()->count();
        }else{
            $rates_avg=0;
        }
        $resturant_modification=null;
        if(ResturantModification::where('resturant_id',$resturant->id)->count()>0) {
            $resturant_modification=ResturantModification::where('resturant_id',$resturant->id)->get()->last();
            return view('admin.resturants.modification',['admin'=>$admin,'resturant'=>$resturant,'resturant_modification'=>$resturant_modification ,'meals'=>$meals,'packages'=>$packages,'rate'=>$rates_avg]);
        }
        return view('admin.resturants.item',['admin'=>$admin,'resturant'=>$resturant ,'meals'=>$meals,'packages'=>$packages,'rate'=>$rates_avg]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin=auth('admin')->user();
        $resturant=Resturant::find($id);
        $cities=City::get();
        $meals = Meal::where('resturant_id', $id)->get();
        $packages = Package::where('resturant_id',$resturant->id)->get();
        return view('admin.resturants.edit',['admin'=>$admin,'resturant'=>$resturant ,'cities'=>$cities,'meals'=>$meals,'packages'=>$packages]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try{
           $resturant = Resturant::find($id);

           $resturant->update([
               'email'             => $request->email,
               'mobile'            => $request->mobile,
               'city_id'           => $request->city_id,
               'status'            => $request->status,
               'language'          =>$request->language,
               'ar'  => [
                   'name'        => $request->name_ar,
                   'description' => $request->description_ar,
               ],
               'logo'                 => $this->updateImage($resturant, $request, 'logo', 'Images/Resturants/Logos', 400, 400),
               'main_image'           => $this->updateImage($resturant, $request, 'main_image', 'Images/Resturants/MainImages', 400, 400),
               'commercial_record'    => $this->updateImage($resturant, $request, 'commercial_record', 'Images/Resturants/CommercialRecords', 400, 400),
               'municipal_licence'    => $this->updateImage($resturant, $request, 'municipal_licence', 'Images/Resturants/MunicipalLicences', 400, 400),
//            'en'  => [
//                'name'        => $request->name_en,
//                'description' => $request->description_en,
//            ]
           ]);
            } catch (\Exception $e) { // It's actually a QueryException but this works too
                if ($e->getCode() == 23000) {
                return  redirect()->back()->withErrors(['msg' => 'رقم الجوال أو الابريد الاكتروني تم اخذه مسبقا  ']);
                }
            }
        return redirect(route('admin_panel.resturants.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resturant = Resturant::find($id);
        (File::exists($resturant->image)) ? File::delete($resturant->image) : Null;
        (File::exists($resturant->logo)) ? File::delete($resturant->logo) : Null;
        (File::exists($resturant->main_image)) ? File::delete($resturant->main_image) : Null;
        foreach ($resturant->images()->pluck('image') as $Image) {
            (File::exists($Image)) ? File::delete($Image) : Null;
        }
        $resturant->delete();

        return redirect()->back();
    }

    public function reject(Request $request,$id){
        $resturant = Resturant::find($id);
        $resturant->update([
            'status'            => '3',
        ]);
        $resturant_m = ResturantModification::where('resturant_id',$resturant->id)->get()->last();

        (File::exists($resturant_m->image)) ? File::delete($resturant_m->image) : Null;
        (File::exists($resturant_m->main_image)) ? File::delete($resturant_m->main_image) : Null;
        (File::exists($resturant_m->logo)) ? File::delete($resturant_m->logo) : Null;
        foreach ($resturant_m->images()->pluck('image') as $Image) {
            (File::exists($Image)) ? File::delete($Image) : Null;
        }
        $resturant_m->delete();
        return redirect(route('admin_panel.resturants.index'));
    }

    public function accept(Request $request,$id){
        $resturant = Resturant::find($id);
        $resturant_m = ResturantModification::where('resturant_id',$resturant->id)->get()->last();

        $resturant->update([
//            'email'             =>$resturant_m->email,
//            'mobile'            =>$resturant_m->mobile,
//            'language'          =>$resturant_m->language,
            'city_id'           => $resturant_m->city_id,
            'latitude'          => $resturant_m->latitude,
            'longitude'         => $resturant_m->longitude,
            'ar'  => [
                'name'        => $resturant_m->translate('ar')->name,
                'description' => $resturant_m->translate('ar')->description,
            ],
            'en'  => [
                'name'        => $resturant_m->translate('en')->name,
                'description' => $resturant_m->translate('en')->description,
            ],
            'status'            => '3'
        ]);

        if($resturant_m->logo){
            $resturant->update([
                'logo'              => $resturant_m->logo
            ]);
        }
        if($resturant_m->main_image){
            (File::exists($resturant->main_image)) ? File::delete($resturant->main_image) : Null;
            $resturant->update([
                'main_image'        => $resturant_m->main_image
            ]);
        }
        $Images = $resturant_m->images;
        if ($Images) {
            foreach ($Images as $Image) {
                ResturantImage::create(['resturant_id' => $resturant->id, 'image' => $Image->image]);
            }
        }

        foreach ($resturant_m->images() as $Image) {
            $Image->delete();
        }
        $resturant_m->delete();
        return redirect(route('admin_panel.resturants.index'));
    }

}

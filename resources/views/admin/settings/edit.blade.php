@extends('admin.dashboard')

@section('content')


    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">الإعدادت </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class=""> الإعدادات </h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">
                                <form class="text-center user-info" method="post" action="{{route('admin_panel.settings.update',1)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> كلفة التوصيل </label>
                                        <input type="number" class="form-control"  value="{{$setting->delivery_cost}}" name="delivery_cost">
                                    </div>


                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> تكلفة الكيلومتر الاإضافي </label>
                                        <input type="number" class="form-control"  value="{{$setting->kilometer_cost}}" name="kilometer_cost">
                                    </div>


                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> المسافة الأساسية </label>
                                        <input type="number" class="form-control"  value="{{$setting->primitive_distance}}" name="primitive_distance">
                                    </div>


                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> رقم التواصل </label>
                                        <input type="tel" class="form-control"  value="{{$setting->mobile_contact}}" name="mobile_contact">
                                    </div>




                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">تعديل</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>


@endsection

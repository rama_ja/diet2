<?php

namespace App\Http\Requests\Api\Client_App\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_id'                         => 'required',
            'resturant_id'                       => 'required',
            'coupon_id'                          => 'nullable',
            'payment_method'                     => 'required|string',
            'delivery_commission'                => 'required',
            'order_details'                      => 'required|string',
            'total_price'                        => 'required',
            'has_discount'                       => 'required|integer',
            'discount_type'                      => 'required_if:has_discount,1|integer',
            'discount_value'                     => 'required_if:has_discount,1|double',
            'total'                              => 'required',
            'meals.*'                            => 'nullable',
            'extras.*'                           => 'nullable',
            'packages.*'                         => 'nullable',
        ];
    }
}

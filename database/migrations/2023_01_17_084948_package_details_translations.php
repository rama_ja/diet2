<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PackageDetailsTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_details_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('package_details_id')->constrained('package_details')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->text('description');
            $table->unique(['package_details_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_details_translations');
    }
}

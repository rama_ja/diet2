<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Client_App\Order\OrderResource;
use App\Http\Resources\Api\Client_App\Package\MyPackageResource;
use App\Http\Resources\Api\Resturant_App\Package\PackageResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use  ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $orders = Order::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All orders of this restaurant', 200);
    }
    public function previousOrders(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $orders = Order::where('resturant_id', $resturant->id)->whereIn('status',['5','6'])->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All previous orders of this restaurant', 200);
    }
    public function currentOrders(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $orders = Order::where('resturant_id', $resturant->id)->whereNotIn('status',['5','6'])->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All current orders of this restaurant', 200);
    }

    public function currentPackages(Request $request){
        $resturant = $request->user('resturant_api');
        $current=[];
        $now=Carbon::now()->toDateTime();
        foreach(Order::where('resturant_id',$resturant->id)->where('status','5')->orderBy('created_at', 'DESC')->get() as $order){
            $packag_end_date=$order->created_at->addDays(7+6 - $order->created_at->dayOfWeek);
            if($packag_end_date > $now){
                foreach ($order->packageOreders as $pac)
                    $current[]=$pac;
            }

        }
        return $this->apiResponse(MyPackageResource::collection($current), 'All current packages', 200);
    }
    public function changeStatus(Request $request, $id)
    {
        $order=Order::find($id)->update(['status' => $request->status]);
        return $this->apiResponse(null, 'Order status changed successfully', 200);
    }

}

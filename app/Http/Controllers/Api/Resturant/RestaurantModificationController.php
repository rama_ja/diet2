<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Resturant\UpdateResturantDetailsRequest;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantImagesResource;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantMofificationResource;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Resturant;
use App\Models\ResturantImage;
use App\Models\ResturantImageModification;
use App\Models\ResturantModification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class RestaurantModificationController extends Controller
{

    use SaveImageTrait, ApiResponseTrait;
    public function updateRestaurantDetails(UpdateResturantDetailsRequest $request)
    {
        $resturant = $request->user('resturant_api');
        $resturant->update([
            'status'            => '2',
        ]);

        if(ResturantModification::where('resturant_id',$resturant->id)->count()>0){

            foreach (ResturantModification::where('resturant_id',$resturant->id)->get() as $res){

                $this->destroy($resturant->id);
            }
        }
        $update_resturant=ResturantModification::create([
            'resturant_id'      =>$resturant->id,
            'email'             =>$resturant->email,
            'mobile'            =>$resturant->mobile,
            'language'          =>$resturant->language,
            'city_id'           => $request->city_id,
            'latitude'          => $request->latitude,
            'longitude'         => $request->longitude,
            'ar'  => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ],
            'commercial_record' => $resturant->commercial_record,
            'municipal_licence' => $resturant->municipal_licence,
            'status'            => '2'
        ]);

        if($request->file('logo')){
            $update_resturant->update([
                'logo'              => $this->saveImage($request->file('logo'), 'Images/Resturants/Logos', 400, 400)
            ]);
        }
        if($request->file('main_image')){
            $update_resturant->update([
                'main_image'        => $this->saveImage($request->file('main_image'), 'Images/Resturants/MainImages', 400, 400)
            ]);
        }
        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Resturants/ResturantImages', 400, 400);
                ResturantImageModification::create(['resturant_modification_id' => $update_resturant->id, 'image' => $image_name_DataBase]);
            }
        }
//        foreach ($resturant->images as $image){
//            ResturantImageModification::create(['resturant_id' => $update_resturant->id, 'image' => $image->image]);
//        }

        return $this->apiResponse(new ResturantMofificationResource($update_resturant), 'watting  admin to approve the changes', 200);
    }
    public function storeImage(Request $request)
    {
        $resturant = $request->user('resturant_api');

        $resturant->update([
            'status'            => '2',
        ]);
        $validator = Validator::make($request->all(), [
            'images'    => 'required|array|max:10',
            'images.*'  => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $final_array = [];

        if(ResturantModification::where('resturant_id',$resturant->id)->count()>0){

            $modif_resturant=ResturantModification::where('resturant_id',$resturant->id)->get()->last();
            $Images = $request->file('images');
            if ($Images) {
                foreach ($Images as $Image) {
                    $image_name_DataBase = $this->saveImages($Image, 'Images/Resturants/ResturantImages', 400, 400);
                    $new_image =  ResturantImageModification::create(['resturant_modification_id' => $modif_resturant->id, 'image' => $image_name_DataBase]);
                    array_push($final_array, $new_image);
                }
            }
        }else{
            $update_resturant=ResturantModification::create([
                'resturant_id'      =>$resturant->id,
                'email'             =>$resturant->email,
                'mobile'            =>$resturant->mobile,
                'language'          =>$resturant->language,
                'city_id'           => $resturant->city_id,
                'latitude'          => $resturant->latitude,
                'longitude'         => $resturant->longitude,
                'ar'  => [
                    'name'          => $resturant->translate('ar')->name,
                    'description'   => $resturant->translate('ar')->description,
                ],
                'en'  => [
                    'name'        => $resturant->translate('en')->name,
                    'description' => $resturant->translate('en')->description,
                ],
                'commercial_record' => $resturant->commercial_record,
                'municipal_licence' => $resturant->municipal_licence,
                'status'            => '2'
            ]);


            $Images = $request->file('images');
            if ($Images) {
                foreach ($Images as $Image) {
                    $image_name_DataBase = $this->saveImages($Image, 'Images/Resturants/ResturantImages', 400, 400);
                    $new_image =  ResturantImageModification::create(['resturant_modification_id' => $update_resturant->id, 'image' => $image_name_DataBase]);
                    array_push($final_array, $new_image);
                }
            }
        }


        return $this->apiResponse(ResturantImagesResource::collection($final_array), 'watting  admin to approve the changes', 200);

    }

    public function destroy($id){
        $resturant = Resturant::find($id);
        $resturant_m = ResturantModification::where('resturant_id',$resturant->id)->get()->last();

       (File::exists($resturant_m->image)) ? File::delete($resturant_m->image) : Null;
       (File::exists($resturant_m->main_image)) ? File::delete($resturant_m->main_image) : Null;
       (File::exists($resturant_m->logo)) ? File::delete($resturant_m->logo) : Null;
       foreach ($resturant_m->images()->pluck('image') as $Image) {
           (File::exists($Image)) ? File::delete($Image) : Null;
       }
        $resturant_m->delete();
       return redirect()->back();


    }


}

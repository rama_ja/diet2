@extends('admin.dashboard')

@section('content')
    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">العملاء </li>
                                    <li class="breadcrumb-item active"> تقييمات العميل </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="table-responsive">
            <table id="myTable1" class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center" scope="col"></th>
                    <th scope="col">اسم المطعم </th>
                    <th scope="col">تقييم المطعم </th>
                    <th scope="col"> تجربة العميل </th>
                    <th scope="col">اسم المندوب </th>
                    <th scope="col">تقييم المندوب </th>
                    <th scope="col"> رقم الطلب </th>
                    <th scope="col"> تقييم الطلب </th>
                    <th scope="col">تاريخ التقييم</th>
                </tr>
                </thead>
                <tbody>

                <?php $counter=1;?>
                @foreach($rates as $rate)

                    <tr>
                        <td>
                            <p class="text-center">{{$counter}}</p>
                            <span class="text-success"></span>
                            <?php $counter++;?>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->resturant->translate('ar')->name}}</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->resturant_evaluation}}/5</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->client_experience}}</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->representative->name}}</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->representative_evaluation}}/5</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->order_id}}</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->order_evaluation}}/5</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">{{$rate->created_at}}</p>
                            <span class="text-success"></span>
                        </td>
                    </tr>

                @endforeach
                </tbody>

            </table>
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                    </div>
                </div>
                <div class="col-sm-12 col-md-7">

                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                        {{ $rates->links() }}

                    </div>
                </div>
            </div>
        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->


    </div>



@endsection

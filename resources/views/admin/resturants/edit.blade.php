@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">المطاعم </li>
                                    <li class="breadcrumb-item active"> تعديل المطعم </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">تعديل معلومات المطعم</h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">
                                <form class="" method="post" action="{{route('admin_panel.resturants.update',$resturant->id)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif
                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label"> الشعار </label><br>
                                        <img alt="avatar" src="/{{$resturant->logo}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="logo">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label"> الصورة الرئيسية </label><br>
                                        <img alt="avatar" src="/{{$resturant->main_image}}"  width="300" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="main_image">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">البريد الالكتروني</label>
                                        <input type="email" class="form-control"  value="{{$resturant->email}}" name="email">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">رقم الجوال</label>
                                        <input type="tel" class="form-control"  value="{{$resturant->mobile}}" name="mobile">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">الاسم باللغة العربية</label>
                                        <input type="text" class="form-control"  value="{{$resturant->translate('ar')->name}}" name="name_ar">
                                    </div>
                                <!--
                <div class="col-md-6">
                    <label for="inputEmail4" class="form-label">الاسم باللغة الأجنبية</label>
                    <input type="text" class="form-control"  value="{{$resturant->translate('en')->name}}">
                </div>
            -->
                                    <div class="col-md-6">
                                        <label  for="inputEmail4" class="form-label">الوصف باللغة العربية</label>
                                        <input type="text" class="form-control"  value="{{$resturant->translate('ar')->description}}" name="description_ar">
                                    </div>
                                <!--     <div  class="col-md-6">
                    <label for="inputEmail4" class="form-label">الوصف باللغة الأجنبية</label>
                    <input type="text" class="form-control"  value="{{$resturant->translate('en')->description}}">
                </div>
          -->
                                    <div class="col-md-6">
                                        <label for="inputState" class="form-label">المدينة</label>
                                        <select class="form-select" name="city_id">
                                            <option selected value="{{$resturant->city->id}}">{{$resturant->city->translate('ar')->name}} </option>
                                            @foreach($cities as $city)
                                                @if($city->id != $resturant->city->id)
                                                    <option value="{{$city->id}}">{{$city->translate('ar')->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputState" class="form-label">اللغة</label>
                                        <select class="form-select" name="language">
                                            @if($resturant->language =='en')
                                                <option selected value="{{$resturant->language}}">الأجنبية </option>
                                                <option value="ar">العربية</option>
                                            @else
                                                <option selected value="{{$resturant->language}}">العربية </option>
                                                <option value="en">الأجنبية</option>
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label"> السجل التجاري </label><br>
                                        <img alt="avatar" src="/{{$resturant->commercial_record}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="commercial_record">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label"> الترخيص </label><br>
                                        <img alt="avatar" src="/{{$resturant->municipal_licence}}"  width="300" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="municipal_licence">
                                    </div>

                                    <div class="row col-md-6">
                                        <label for="inputEmail4" class="form-label">ساعات العمل </label>
                                        <div class="col-md-4">
                                            <label for="inputState" class="form-label">اليوم</label>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="inputState" class="form-label">وقت الفتح</label>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="inputState" class="form-label">وقت الإغلاق</label>
                                        </div>
                                        @foreach($resturant->workingHours as $work)
                                            <div class="col-md-4">
                                                <p>
                                                    {{$work->day->translate('ar')->name}}
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p>
                                                    {{$work->opening_hour}}
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p>
                                                    {{$work->closing_hour}}
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>

                                <!--
                <div class="col-md-6">
                    <label for="inputEmail4" class="form-label">صور المطعم </label>
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" >
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="/{{$resturant->main_image}}" alt="First slide" width="300" height="200">
                            </div>
                            @foreach($resturant->images as $img)
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="/{{$img->image}}" alt="Second slide" width="300" height="200">
                                </div>
                            @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </a>
                                </div>
                            </div>
-->
                                    <div class="col-12">
                                        <div class="row">
                                            <div id="splider_MultipleSlider" class="col-lg-12 col-md-12 layout-spacing">
                                                <div class="statbox widget box box-shadow">
                                                    <div class="widget-header">
                                                        <div class="row">
                                                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                <h4>صور المطعم </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget-content widget-content-area">
                                                        <div class="position-relative">
                                                            <div class="container" style="max-width: 700px">
                                                                <div class="splide-multiple">
                                                                    <div class="splide__track">
                                                                        <ul class="splide__list">
                                                                            @foreach($resturant->images as $img)
                                                                                <li class="splide__slide">
                                                                                    <img alt="slider-image" class="img-fluid" src="/{{$img->image}}" width="100" height="100">
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div id="splider_MultipleSlider" class="col-lg-12 col-md-12 layout-spacing">
                                                <div class="statbox widget box box-shadow">
                                                    <div class="widget-header">
                                                        <div class="row">
                                                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                <h4>صور الوجبات </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget-content widget-content-area">
                                                        <div class="position-relative">
                                                            <div class="container" style="max-width: 700px">
                                                                <div class="splide-multiple">
                                                                    <div class="splide__track">
                                                                        <ul class="splide__list">
                                                                            @foreach($meals as $meal)
                                                                                <li class="splide__slide">
                                                                                    <img alt="slider-image" class="img-fluid" src="/{{$meal->main_image}}" width="100" height="100">
                                                                                </li>
                                                                            @endforeach

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div id="splider_MultipleSlider" class="col-lg-12 col-md-12 layout-spacing">
                                                <div class="statbox widget box box-shadow">
                                                    <div class="widget-header">
                                                        <div class="row">
                                                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                <h4>صور الباقات </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget-content widget-content-area">
                                                        <div class="position-relative">
                                                            <div class="container" style="max-width: 700px">
                                                                <div class="splide-multiple">
                                                                    <div class="splide__track">
                                                                        <ul class="splide__list">
                                                                            @foreach($packages as $package)
                                                                                <li class="splide__slide">
                                                                                    <img alt="slider-image" class="img-fluid" src="/{{$package->main_image}}" width="100" height="100">
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputState" class="form-label">الحالة</label>
                                        <select class="form-select" name="status">
                                            @if($resturant->status ==1)
                                                <option selected value="1">بانتظار القبول </option>
                                                <option value="2">بانتظار قبول التعديلات</option>
                                                <option value="3">مقبول</option>
                                                <option value="4">مرفوض</option>
                                            @elseif($resturant->status ==2)
                                                <option value="1">بانتظار القبول </option>
                                                <option selected value="2">بانتظار قبول التعديلات</option>
                                                <option value="3">مقبول</option>
                                                <option value="4">مرفوض</option>
                                            @elseif($resturant->status ==3)
                                                <option value="1">بانتظار القبول </option>
                                                <option value="2">بانتظار قبول التعديلات</option>
                                                <option selected value="3">مقبول</option>
                                                <option value="4">مرفوض</option>
                                            @elseif($resturant->status ==4)
                                                <option value="1">بانتظار القبول </option>
                                                <option value="2">بانتظار قبول التعديلات</option>
                                                <option value="3">مقبول</option>
                                                <option selected value="4">مرفوض</option>
                                            @endif
                                        </select>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">تعديل</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection

<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Order\StoreOrderRequest;
use App\Http\Requests\Api\Client_App\Package\StoreRateRequest;
use App\Http\Resources\Api\Client_App\Meal\MealRatingResource;
use App\Http\Resources\Api\Client_App\Order\OrderResource;
use App\Http\Resources\Api\Client_App\Order\SettingResource;
use App\Http\Resources\Api\Resturant_App\Coupon\CouponResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Coupon;
use App\Models\DetailChangeOrder;
use App\Models\ExtraOrder;
use App\Models\MealOrder;
use App\Models\Order;
use App\Models\Package;
use App\Models\PackageDetails;
use App\Models\PackageOrder;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use  ApiResponseTrait;

    public function index(Request $request)
    {
        $client = $request->user('client_api');
        $orders = Order::where('client_id', $client->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All orders for this client ', 200);
    }
    public function previousOrders(Request $request)
    {
        $client = $request->user('client_api');
        $orders = Order::where('client_id', $client->id)->whereIn('status',['5','6'])->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All previous orders for this client ', 200);
    }
    public function currentOrders(Request $request)
    {
        $client = $request->user('client_api');
        $orders = Order::where('client_id', $client->id)->whereNotIn('status',['5','6'])->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All current orders for this client ', 200);
    }
//    public function previousPackages(Request $request)
//    {
//        $client = $request->user('client_api');
//        $orders = Order::where('client_id', $client->id)->where('status','5')->orderBy('created_at', 'DESC')->paginate(20);
//        foreach ($orders as $order){
//            foreach ($order->packages as $package){
//                $details=PackageDetails::where('Package_id',$package->id)->get();
//
//            }
//        }
//        return $this->apiResponse(OrderResource::collection($orders), 'All previous orders for this client ', 200);
//    }
//    public function addEvaluation(StoreRateRequest $request,$id){
//        $client = $request->user('client_api');
//        $order=Order::find($id);
//        $evaluation=OrderRating::create([
//            'order_id'            =>$order->id,
//            'client_id'          =>$client->id,
//            'evaluation'         =>$request->evaluation
//        ]);
//
//        return $this->apiResponse(new MealRatingResource($evaluation), 'The evaluation has been added successfully ', 200);
//    }

//    public function getRates($id){
//
//        $order=Order::find($id);
//        $rates=OrderRating::where('order_id',$order->id)->orderBy('created_at', 'DESC')->paginate(20);
//        return $this->apiResponse(MealRatingResource::collection($rates), 'Order rates ', 200);
//    }
    public function store(StoreOrderRequest $request)
    {
        $client = $request->user('client_api');
        $order = Order::create([
            'client_id'                => $client->id,
            'resturant_id'             => $request->resturant_id,
            'address_id'               => $request->address_id,
            'coupon_id'                => $request->coupon_id,
            'payment_method'           => $request->payment_method,
            'delivery_commission'      => $request->delivery_commission,
            'order_details'            => $request->order_details,
            'total_price'              => $request->total_price,
            'has_discount'             => $request->has_discount,
            'discount_type'            => $request->discount_type,
            'discount_value'           => $request->discount_value,
            'total'                    => $request->total
        ]);



        $meals=$request->meals;
        if($meals){
            foreach ($meals as $meal){

                MealOrder::create([
                    'meal_id'           =>$meal['meal_id'],
                    'order_id'          =>$order->id,
                    'quantity'          =>$meal['quantity'],
                ]);
            }
        }

        $extras=$request->extras;
        if($extras){
            foreach ($extras as $extra){
                ExtraOrder::create([
                    'extra_id'          =>$extra['extra_id'],
                    'order_id'          =>$order->id,
                    'quantity'          =>$extra['quantity'],
                ]);
            }
        }

        $packages=$request->packages;
        if($packages){
            foreach ($packages as $package){
                PackageOrder::create([
                    'package_id'        =>$package['package_id'],
                    'order_id'          =>$order->id,
                    'quantity'          =>$package['quantity'],
                ]);
                foreach ($package['details'] as $package){
                    DetailChangeOrder::create([
                        'package_detail_id'   =>$package['detail_id'] ,
                        'order_id'             =>$order->id,
                        'delivery_time'        =>$package['time']
                    ]);
                }
            }
        }


        return $this->apiResponse(new OrderResource($order), 'The order has been created successfully', 200);
    }


    public function checkCoupon(Request $request){
        $client = $request->user('client_api');
        $coupon=Coupon::where('code',$request->code)->first();


        if(!$coupon){
            return $this->apiResponse(null, 'This code doesnt match any coupon', 200);
        }
        if($coupon->resturant_id !=$request->resturant_id){
            return $this->apiResponse(null, 'This code not valiad to this resturant', 200);
        }
        $valid_date = Carbon::now()->between($coupon->start_date, $coupon->end_date);
        if(!$valid_date){
            return $this->apiResponse(null, 'This code is expired', 200);
        }

        $taken=$client->coupons()->where('id',$coupon->id)->first();
        if($taken){
            return $this->apiResponse(null, 'This code is used before', 200);
        }

        $client->coupons()->attach($coupon);
        return $this->apiResponse(new CouponResource($coupon), 'The coupon matches', 200);
    }

    public function getSetting(){

        $setting=Setting::get()->first();
        return $this->apiResponse(new SettingResource($setting), 'The settings', 200);

    }


}

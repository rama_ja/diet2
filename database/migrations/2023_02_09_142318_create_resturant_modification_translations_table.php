<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResturantModificationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resturant_modification_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('resturant_modification_id')->constrained('resturant_modifications')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->unique(['resturant_modification_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resturant_modification_translations');
    }
}

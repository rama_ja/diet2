<?php

namespace App\Http\Resources\Api\Resturant_App\Meal;

use Illuminate\Http\Resources\Json\JsonResource;

class MealTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name_ar' => $this->translate('ar')->name,
            'name_en' => $this->translate('en')->name,
            'status'  => $this->status
        ];
    }
}

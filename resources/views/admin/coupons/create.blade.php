@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">أكواد الحسم</li>
                                    <li class="breadcrumb-item active"> إضافة </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">إضافة كود حسم</h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">

                                <form class="row g-3" method="post" action="{{route('admin_panel.coupons.store')}}" enctype="multipart/form-data" >
                                    @method('POST')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">كود الحسم </label>
                                        <input type="text" class="form-control"   name="code">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputState" class="form-label">المطعم التابع له</label>
                                        <select class="form-select" name="resturant_id">
                                            @foreach($resturants as $resturant)
                                                <option value="{{$resturant->id}}">{{$resturant->translate('ar')->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> الحد الأدنى للفاتورة </label>
                                        <input type="number" class="form-control"   name="minimum_price">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> مقدار الحسم </label>
                                        <input type="number" class="form-control"   name="discount_amount">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputState" class="form-label"> نوع الحسم </label>
                                        <select class="form-select" name="discount_type">
                                            <option value="0">نسبة مئوية </option>
                                            <option value="1">مقدار عددي </option>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> تاريخ البدء </label>
                                        <input type="date" class="form-control"   name="start_date">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> تاريخ الانتهاء </label>
                                        <input type="date" class="form-control"   name="end_date">
                                    </div>


                                    <div class="col-6">
                                        <div class="text-center">
                                         <button type="submit" class="btn btn-primary">إضافة </button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection

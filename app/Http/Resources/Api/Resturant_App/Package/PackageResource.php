<?php

namespace App\Http\Resources\Api\Resturant_App\Package;

use App\Http\Resources\Api\Client_App\Package\PackageRatingResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $sum=0;
        foreach ($this->rates as $rate){
            $sum+=$rate->evaluation;
        }
        if($this->rates()->count()>0){
            $rates_avg=$sum/$this->rates()->count();
        }else{
            $rates_avg=0;
        }
        return [
            'id'                  => $this->id,
            'resturant_id'        => $this->resturant_id,
            'resturant_latitude'  => $this->resturant->latitude,
            'resturant_longitude' => $this->resturant->longitude,
            'name_ar'             => $this->translate('ar')->name,
            'name_en'             => $this->translate('en')->name,
            'price'               => $this->price,
            'total_calories'      => $this->total_calories,
            'carbohydrate'        => $this->carbohydrate,
            'protein'             => $this->protein,
            'fats'                => $this->fats,
            'main_image'          => $this->main_image,
            'status'              => $this->status,
            'description_ar'      => $this->translate('ar')->description,
            'description_en'      => $this->translate('en')->description,
            'images'              => $this->images ?  PackageImagesResource::collection($this->images) : null,
            'details'             => $this->details ? PackageDetailsResource::collection($this->details) : null,
            'rates'               => $rates_avg,
            'rates_count'         => $this->rates()->count(),
        ];
    }
}

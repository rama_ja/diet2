<?php

namespace App\Http\Controllers\Api\Representative;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Client_App\Order\OrderResource;
use App\Http\Resources\Api\Representative_App\MessageResource;
use App\Http\Resources\Api\Representative_App\RepresentativeRejectOrderResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Chatting;
use App\Models\Order;
use App\Models\RepresentativeRejectOrder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use  ApiResponseTrait;
    public function index(Request $request)
    {
        $representative = $request->user('representative_api');
        $rejectedOrders=$representative->rejectedOrder;
        foreach ($rejectedOrders as $orderIds) {
            $data[] = $orderIds->id;
        }
        $orders = Order::where('representative_id', null)
            ->where('status','2')
            ->whereNotIn('id',$data)
            ->orderBy('created_at', 'DESC')->paginate(20);


        return $this->apiResponse(OrderResource::collection($orders), 'All orders for this representative ', 200);
    }
    public function rejectOrder(Request $request,$id)
    {
        $representative = $request->user('representative_api');
        $order=Order::find($id);
        $reject=RepresentativeRejectOrder::create([
            'representative_id'      =>$representative->id,
            'order_id'               =>$order->id,
            'cause'                  =>$request->cause
        ]);
        return $this->apiResponse(new RepresentativeRejectOrderResource($reject), 'Order has been rejected successfully', 200);
    }
    public function changeStatus(Request $request, $id)
    {
        $representative = $request->user('representative_api');
        if($request->status=='2'){
            $order=Order::find($id);
            $order->update([
                'representative_id' =>$representative->id
            ]);
        }
        Order::find($id)->update(['status' => $request->status]);
        return $this->apiResponse(null, 'Order status changed successfully', 200);
    }



}

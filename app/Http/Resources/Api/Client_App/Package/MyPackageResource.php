<?php

namespace App\Http\Resources\Api\Client_App\Package;

use App\Http\Resources\Api\Client_App\Order\DetailChangeOrderResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MyPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $sum=0;
        foreach ($this->package->resturant->rates as $rate){
            $sum+=$rate->resturant_evaluation;
        }
        if($this->package->resturant->rates()->count()>0){
            $rates_avg=$sum/$this->package->resturant->rates()->count();
        }else{
            $rates_avg=0;
        }
        return [
            'id'                  => $this->id,
            'name_ar'             => $this->package->translate('ar')->name,
            'name_en'             => $this->package->translate('en')->name,
            'resturant_name_en'   => $this->package->resturant->translate('en')->name,
            'resturant_name_ar'   => $this->package->resturant->translate('ar')->name,
            'resturant_logo'      => $this->package->resturant->logo,
            'resturant_rate'      => $rates_avg,
            'main_image'          => $this->package->main_image,
            'details'             => $this->order->detailChange ? DetailChangeOrderResource::collection($this->order->detailChange) : null,
            'order_date'          => $this->order->created_at,
            'order_id'            => $this->order->id,
            'quantity'            => $this->quantity,
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Client\UpdatePasswordRequest;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Order;
use App\Models\Rating;
use App\Models\Representative;
use App\Models\Resturant;
use App\Models\ResturantRating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    use SaveImageTrait,ApiResponseTrait;

    public function __construct()
    {

        $this->middleware('auth:admin', ['except' => ['login','loginView']]);
    }
    public function loginView()
    {
        return view('admin.signin');
    }
     public function analys()
    {
        $admin=auth('admin')->user();
        $clients=Client::get();
        $resturants=Resturant::get();
        $representatives=Representative::get();
        $orders=Order::get();
        $rates=Rating::get();
        return view('admin.layouts.analys',['admin'=>$admin,'clients'=>$clients,'resturants'=>$resturants,'representatives'=>$representatives,'orders'=>$orders,'rates'=>$rates]);
    }

    public function index()
    {
        $admin=auth('admin')->user();;
        return view('admin.profile.index',['admin'=>$admin]);
    }
    public function edit($id)
    {
        $admin=auth('admin')->user();;
        return view('admin.profile.edit',['admin'=>$admin]);
    }

    public function update(Request $request,$id)
    {
        $admin=auth('admin')->user();

        $admin->update([
            'name'              => $request->name,
            'email'             => $request->email
        ]);

        if($request->file('personal_image')){
            if( preg_match("/\b(" . "avatar" . ")\b/i", $admin->personal_image) ){

                $admin->update([
                    'personal_image' => $this->saveImage($request->file('personal_image'), 'Images/Admins', 400, 400),
                ]);
            } else {
                $admin->update([
                    'personal_image'    =>  $this->updateImage($admin, $request, 'personal_image', 'Images/Admins', 400, 400)
                ]);
            }
        }

        return redirect(route('admin_panel.profile.index'));
    }

    public function editPassword()
    {
        $admin=auth('admin')->user();;
        return view('admin.profile.editPassword',['admin'=>$admin]);
    }

    public function updatePassword(Request $request)
    {
        $admin=auth('admin')->user();


        $rules = [
            'password' => ['required', 'confirmed', Password::min(8)->numbers()->symbols()]
        ];

        $customMessages = [
            'password.required' => 'خفل كلمة المورو لا يجب أن يكون فارغا',
            'password.confirmed' => ' تأكيد كلمة المرور غير مطابق',
            'password.min'   => 'يجب ان تكون كلمة المرور على الأقل مكونة من ثماني محارف'
        ];
        if ( ! preg_match('/(\p{Ll}+.*\p{Lu})|(\p{Lu}+.*\p{Ll})/u', $request->password)) {
             return  redirect()->back()->withErrors(['msg' => 'كلمة المرور يجب ان تحوي محرف كبير و محرف صغير  ']);

        }

        if ( ! preg_match('/\pL/u', $request->password)) {
            return  redirect()->back()->withErrors(['msg' => 'كلمة المرور يجب ان تحوي حرف واحد على الاأقل ']);
        }

        if ( ! preg_match('/\p{Z}|\p{S}|\p{P}/u', $request->password)) {
            return  redirect()->back()->withErrors(['msg' => 'كلمة المرور يجب ان تحوي رمز واحد على الاأقل ']);
        }

        if (  ! preg_match('/\pN/u', $request->password)) {
            return  redirect()->back()->withErrors(['msg' => 'كلمة المرور يجب ان تحوي رقم واحد على الأقل ']);
        }

        $this->validate($request, $rules, $customMessages);

        if (!auth('admin')->attempt(['email'=>$admin->email,'password'=>$request->old_password])) {

            return  redirect()->back()->withErrors(['msg' => 'كلمة المرور القديمة غير صحيحة']);
        }
        else{
            $admin->update([
                'password'          => Hash::make($request->password)
            ]);

            return redirect('/admin_panel');
        }


    }

//    public function login(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'email'    => 'required',
//            'password'  => 'required',
//        ]);
//        if ($validator->fails()) {
//            return response()->json($validator->errors(), 422);
//        }
//        if (!$token = auth('admin_api')->attempt($validator->validated())) {
//            return response()->json(['error' => 'Unauthorized'], 401);
//        }
////        return $this->createNewToken($token);
//        return redirect('/admin_panel');
//    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if (auth('admin')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect('/admin_panel');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }

    public function register(Request $request)
    {
            $admin = Admin::create([
                'name'              => $request->name,
                'email'             => $request->email,
                'password'          => Hash::make($request->password),
                ]);

        if($request->file('personal_image')) {
            $admin->update([
                'personal_image'  => $this->saveImage($request->file('personal_image'), 'Images/Admins', 400, 400),
            ]);
        }

        $request->session()->regenerate();
        return redirect('/admin_panel');
    }




    public function logout(Request $request)
    {
        auth('admin')->logout();
        return redirect(route('admin_panel.login'));
    }

//    public function refresh()
//    {
//        return $this->createNewToken(auth('admin_api')->refresh());
//    }
//
//    protected function createNewToken($token)
//    {
//        return response()->json([
//            'access_token'  => $token,
//            'token_type'    => 'bearer',
//            'expires_in'    => auth('admin_api')->factory()->getTTL() * 60,
//            'admin'        => auth('admin_api')->user()
//        ]);
//    }

}

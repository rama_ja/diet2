<?php

namespace App\Http\Resources\Api\Client_App\Client;

use App\Models\Order;
use App\Models\OrderRating;
use App\Models\Rating;
use App\Models\RepresentativeRating;
use App\Models\ResturantRating;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $orders_count=Order::where('client_id',$this->id)->count();
        $payments=Order::where('client_id',$this->id)->sum('total');

        $rates_count=Rating::where('client_id',$this->id)->count();

        return [
            'id'                  => $this->id,
            'name'                => $this->name,
            'email'               => $this->email,
            'mobile'              => $this->mobile,
            'city_id'             => $this->city_id,
            'personal_image'      => $this->personal_image,
            'orders_count'        => $orders_count,
            'payments'            => $payments,
            'rates_count'         => $rates_count
        ];
    }
}

<?php

namespace App\Http\Resources\Api\Client_App\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
        'id'                   => $this->id,
        'delivery_cost'        => $this->delivery_cost,
        'kilometer_cost'       => $this->kilometer_cost,
        'primitive_distance'   => $this->primitive_distance,
        'mobile_contact'       => $this->mobile_contact,
        ];
    }

}

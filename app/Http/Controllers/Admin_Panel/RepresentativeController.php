<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Http\Traits\SaveImageTrait;
use App\Models\Admin;
use App\Models\City;
use App\Models\Meal;
use App\Models\Package;
use App\Models\Representative;
use App\Models\Resturant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class RepresentativeController extends Controller
{
    use SaveImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $admin=auth('admin')->user();

//        if($request->ajax())
//        {
//            $output="";
//            $representatives=Representative::where('name','LIKE','%'.$request->search."%")
//                ->orWhere('email','LIKE','%'.$request->search."%")->orderBy('created_at', 'DESC')->paginate(3,['*'],'res');
//            if($representatives)
//            {$counter=1;
//
//                $output.='
// <table id="myTable1" class="table table-striped table-bordered table-sm">
//                <thead>
//                <tr>
//                    <th class="text-center" scope="col"></th>
//                    <th scope="col">اسم المندوب</th>
//                    <th scope="col">تاريخ الانضمام</th>
//                    <th class="text-center" scope="col">المطعم التابع له </th>
//                    <th class="text-center" scope="col">الحالة</th>
//                    <th class="text-center" scope="col"></th>
//                </tr>
//                </thead>
//                <tbody>';
//                foreach ($representatives as $key => $representative) {
//                    $output.=' <tr>
//                               <td>
//                            <p class="text-center">'.$counter.'</p>
//                            <span class="text-success"></span>
//                        </td>
//                        <td>
//                             <div class="media">
//                                <div class="avatar me-2">
//                                    <img alt="avatar" src="/'.$representative->personal_image.'" class="rounded-circle" />
//                                </div>
//                                <div class="media-body align-self-center">
//                                    <h6 class="mb-0">'.$representative->name.'</h6>
//                                    <span>'.$representative->email.'</span>
//                                </div>
//                            </div>
//                        </td>
//                        <td>
//                             <p class="mb-0">'.$representative->created_at->toDateString().'</p>
//                            <span class="text-success"></span>
//                        </td>     <td class="text-center">
//                                    <span >';
//                    if($representative->resturant)
//                        $output.= $representative->resturant->translate('ar')->name;
//                    else
//                        $output.=  'لايوجد';
//
//                    $output.='</span>
//                        </td>
//                        <td class="text-center">
//                                    <span class="badge badge-light-success">';
//                    if($representative->status ==1)
//                        $output.='مقبول';
//                    elseif($representative->status ==2)
//                        $output.= 'غير مقبول';
//
//                    $output.=' </span>
//                        </td>
//
//                        <td class="text-center">
//                            <div class="action-btns">
//                                <a href="'.route('admin_panel.representatives.show',$representative->id).'" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
//                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
//                                </a>
//                                <a href="'.route('admin_panel.representatives.edit',$representative->id).'" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
//                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
//                                </a>
//                                <a href="/admin_panel/representative_del/'.$representative->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
//                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
//                                </a>
//
//                            </div>
//                        </td>'.
//                        '</tr>';
//
//                    $counter++;
//
//                }
//                $output.='  </tbody>
//
//            </table>
//            <div class="row">
//                <div class="col-sm-12 col-md-5">
//                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
//
//                    </div>
//                </div>
//                <div class="col-sm-12 col-md-7">
//
//                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
//
//
//                        '.$representatives->links() .'
//                    </div>
//                </div>
//            </div>
//
//';
//                return Response($output);
////                $admin=auth('admin')->user();
////                return view('admin.representatives.result',['admin'=>$admin,'representatives'=>$representatives]);
//            }
//        }
        $representative=Representative::orderBy('created_at', 'DESC')->paginate(15);
        return view('admin.representatives.index',['admin'=>$admin,'representatives'=>$representative]);
    }

    public function search(Request $request)
    {
        if($request->ajax())
        {
            $output="";
            $representatives=Representative::where('name','LIKE','%'.$request->search."%")
                ->orWhere('email','LIKE','%'.$request->search."%")->orderBy('created_at', 'DESC')->get();
            if($representatives)
            {$counter=1;

                $output.='
 <table id="myTable1" class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center" scope="col"></th>
                    <th scope="col">اسم المندوب</th>
                    <th scope="col">تاريخ الانضمام</th>
                    <th class="text-center" scope="col">المطعم التابع له </th>
                    <th class="text-center" scope="col">الحالة</th>
                    <th class="text-center" scope="col"></th>
                </tr>
                </thead>
                <tbody>';
                foreach ($representatives as $key => $representative) {
                    $output.=' <tr>
                               <td>
                            <p class="text-center">'.$counter.'</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                             <div class="media">
                                <div class="avatar me-2">
                                    <img alt="avatar" src="/'.$representative->personal_image.'" class="rounded-circle" />
                                </div>
                                <div class="media-body align-self-center">
                                    <h6 class="mb-0">'.$representative->name.'</h6>
                                    <span>'.$representative->email.'</span>
                                </div>
                            </div>
                        </td>
                        <td>
                             <p class="mb-0">'.$representative->created_at->toDateString().'</p>
                            <span class="text-success"></span>
                        </td>     <td class="text-center">
                                    <span >';
                    if($representative->resturant)
                        $output.= $representative->resturant->translate('ar')->name;
                    else
                        $output.=  'لايوجد';

                    $output.='</span>
                        </td>
                        <td class="text-center">
                                    <span class="badge badge-light-success">';
                    if($representative->status ==1)
                        $output.='مقبول';
                    elseif($representative->status ==2)
                        $output.= 'غير مقبول';

                    $output.=' </span>
                        </td>
                       
                        <td class="text-center">
                            <div class="action-btns">
                                <a href="'.route('admin_panel.representatives.show',$representative->id).'" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </a>
                                <a href="'.route('admin_panel.representatives.edit',$representative->id).'" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                </a>
                                <a href="/admin_panel/representative_del/'.$representative->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </a>

                            </div>
                        </td>'.
                        '</tr>';

                    $counter++;

                }
                $output.='  </tbody>

            </table>
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                    </div>
                </div>
                <div class="col-sm-12 col-md-7">

                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
 
                
                     
                    </div>
                </div>
            </div>

';
                return Response($output);
//                $admin=auth('admin')->user();
//                return view('admin.representatives.result',['admin'=>$admin,'representatives'=>$representatives]);
            }
        }
        $admin=auth('admin')->user();
        $representative=Representative::orderBy('created_at', 'DESC')->paginate(15);
        return view('admin.representatives.index',['admin'=>$admin,'representatives'=>$representative]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin=auth('admin')->user();
        $representative=Representative::find($id);
        return view('admin.representatives.item',['admin'=>$admin,'representative'=>$representative ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin=auth('admin')->user();
        $representative=Representative::find($id);
        $cities=City::get();
        $resturants=Resturant::get();
        return view('admin.representatives.edit',['admin'=>$admin,'representative'=>$representative  ,'cities'=>$cities,'resturants'=>$resturants]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $representative = Representative::find($id);

            $representative->update([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'city_id' => $request->city_id,
                'status' => $request->status,
                'vehicle_type' => $request->vehicle_type,
                'vehicle_brand' => $request->vehicle_brand,
                'type' => $request->type,
                'vehicle_model' => $request->vehicle_model,
                'vehicle_number' => $request->vehicle_number,
                'license_image' => $this->updateImage($representative, $request, 'license_image', 'Images/Representatives/LicenseImage', 400, 400),
                'form_image' => $this->updateImage($representative, $request, 'form_image', 'Images/Representatives/FormImage', 400, 400)

            ]);

            if($request->file('personal_image')){
                if( preg_match("/\b(" . "avatar" . ")\b/i", $representative->personal_image) ){

                    $representative->update([
                        'personal_image' => $this->saveImage($request->file('personal_image'), 'Images/Representatives/PersonalImage', 400, 400),
                    ]);
                } else {
                    $representative->update([
                        'personal_image' => $this->updateImage($representative, $request, 'personal_image', 'Images/Representatives/PersonalImage', 400, 400),
                    ]);
                }
            }
            } catch (\Exception $e) { // It's actually a QueryException but this works too
                if ($e->getCode() == 23000) {
                return  redirect()->back()->withErrors(['msg' => 'رقم الجوال أو الابريد الاكتروني تم اخذه مسبقا  ']);
                 }
             }
        return redirect(route('admin_panel.representatives.index'));

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $representative=Representative::find($id);
        (File::exists($representative->image)) ? File::delete($representative->image) : Null;
         if( !preg_match("/\b(" . "avatar" . ")\b/i", $representative->personal_image) ) {
             (File::exists($representative->personal_image)) ? File::delete($representative->personal_image) : Null;
         }
        (File::exists($representative->license_image)) ? File::delete($representative->license_image) : Null;
        (File::exists($representative->form_image)) ? File::delete($representative->form_image) : Null;
        foreach ($representative->images()->pluck('image') as $Image) {
            (File::exists($Image)) ? File::delete($Image) : Null;
        }
        $representative->delete();

        return redirect()->back();
    }
}

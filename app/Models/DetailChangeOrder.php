<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailChangeOrder extends Model
{
    use HasFactory;
    protected $fillable = ['package_detail_id', 'order_id','delivery_time'];

    public function package_detail()
    {
        return $this->belongsTo(PackageDetails::class);
    }

}

<?php

namespace App\Http\Resources\Api\Client_APP\Rating;

use Illuminate\Http\Resources\Json\JsonResource;

class RatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                       => $this->id,
            'representative_evaluation'=> $this->representative_evaluation,
            'order_evaluation'         => $this->order_evaluation,
            'resturant_evaluation'     => $this->resturant_evaluation,
            'client_experience'        => $this->client_experience,
            'client_name'              => $this->client->name,
            'date'                     => $this->created_at
        ];
    }
}

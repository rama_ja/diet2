@extends('admin.dashboard')

@section('content')
    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active">المطاعم </li>

                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->

        <div class="simple-tab">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">جميع المطاعم</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link " id="new-tab" data-bs-toggle="tab" data-bs-target="#new-tab-pane" type="button" role="tab" aria-controls="new-tab-pane" aria-selected="false">  طلبات الانضمام</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link " id="edit-tab" data-bs-toggle="tab" data-bs-target="#edit-tab-pane" type="button" role="tab" aria-controls="edit-tab-pane" aria-selected="false">طلبات التعديل</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link " id="accepted-tab" data-bs-toggle="tab" data-bs-target="#accepted-tab-pane" type="button" role="tab" aria-controls="accepted-tab-pane" aria-selected="false">المطاعم المقبولة</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link " id="rejected-tab" data-bs-toggle="tab" data-bs-target="#rejected-tab-pane" type="button" role="tab" aria-controls="rejected-tab-pane" aria-selected="false">المطاعم المرفوضة</button>
                </li>
            </ul>
            <br>

            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                    <div class="text-center form-group">
                        <label>بحث حسب البريد الالكتروني او الاسم باللغة العربية : </label>
                        <input type="text" class="form-controller" id="search_all" name="search"></input>
                    </div>
                    <div class="table-responsive" id="t1">

                        <table id="myTable1" class="table table-striped table-bordered table-sm">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col"></th>
                                <th scope="col">اسم المطعم</th>
                                <th scope="col">تاريخ الانضمام</th>
                                <th class="text-center" scope="col">الحالة</th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $counter=1;?>
                            @foreach($resturants as $res)
{{--                                @if($res->status !='0')--}}
                                    <tr>
                                        <td>
                                            <p class="text-center">{{$counter}}</p>
                                            <span class="text-success"></span>
                                            <?php $counter++;?>
                                        </td>
                                        <td>
                                            <div class="media">
                                                <div class="avatar me-2">
                                                    <img alt="avatar" src="/{{$res->logo}}" class="rounded-circle" />
                                                </div>
                                                <div class="media-body align-self-center">
                                                    <h6 class="mb-0">{{$res->translate('ar')->name}}</h6>
                                                    <span>{{$res->email}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{$res->created_at->toDateString()}}</p>
                                            <span class="text-success"></span>
                                        </td>
                                        <td class="text-center">
                                    <span class="badge badge-light-success">
                                        @if($res->status ==1)
                                            بانتظار القبول
                                        @elseif($res->status ==2)
                                            بانتظار قبول التعديلات
                                        @elseif($res->status ==3)
                                            مقبول
                                        @elseif($res->status ==4)
                                            مرفوض
                                        @endif
                                    </span>
                                        </td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="{{route('admin_panel.resturants.show',$res->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                </a>
                                                <a href="{{route('admin_panel.resturants.edit',$res->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                </a>
                                                <a href="/admin_panel/resturant_del/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>
                                                @if($res->status =='2' )
                                                    <a href="/admin_panel/reject/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="رفض">
                                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                             width="100" height="100"
                                                             viewBox="0 0 50 50">
                                                            <path d="M 25 2 C 12.309534 2 2 12.309534 2 25 C 2 37.690466 12.309534 48 25 48 C 37.690466 48 48 37.690466 48 25 C 48 12.309534 37.690466 2 25 2 z M 25 4 C 36.609534 4 46 13.390466 46 25 C 46 36.609534 36.609534 46 25 46 C 13.390466 46 4 36.609534 4 25 C 4 13.390466 13.390466 4 25 4 z M 32.990234 15.986328 A 1.0001 1.0001 0 0 0 32.292969 16.292969 L 25 23.585938 L 17.707031 16.292969 A 1.0001 1.0001 0 0 0 16.990234 15.990234 A 1.0001 1.0001 0 0 0 16.292969 17.707031 L 23.585938 25 L 16.292969 32.292969 A 1.0001 1.0001 0 1 0 17.707031 33.707031 L 25 26.414062 L 32.292969 33.707031 A 1.0001 1.0001 0 1 0 33.707031 32.292969 L 26.414062 25 L 33.707031 17.707031 A 1.0001 1.0001 0 0 0 32.990234 15.986328 z"></path>
                                                        </svg>
                                                    </a>
                                                    <a href="/admin_panel/accept/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="قبول">
                                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                             width="100" height="100"
                                                             viewBox="0 0 128 128">
                                                            <path d="M 64 6 C 32 6 6 32 6 64 C 6 96 32 122 64 122 C 96 122 122 96 122 64 C 122 32 96 6 64 6 z M 64 12 C 92.7 12 116 35.3 116 64 C 116 92.7 92.7 116 64 116 C 35.3 116 12 92.7 12 64 C 12 35.3 35.3 12 64 12 z M 85.037109 48.949219 C 84.274609 48.974219 83.500391 49.300391 82.900391 49.900391 L 62 71.599609 L 51.099609 59.900391 C 49.999609 58.700391 48.100391 58.599219 46.900391 59.699219 C 45.700391 60.799219 45.599219 62.700391 46.699219 63.900391 L 59.800781 78 C 60.400781 78.6 61.1 79 62 79 C 62.8 79 63.599219 78.699609 64.199219 78.099609 L 87.199219 54 C 88.299219 52.8 88.299609 50.900781 87.099609 49.800781 C 86.549609 49.200781 85.799609 48.924219 85.037109 48.949219 z"></path>
                                                        </svg>
                                                    </a>
                                                @endif

                                            </div>
                                        </td>
                                    </tr>
{{--                                @endif--}}
                            @endforeach
                            </tbody>

                        </table>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">

                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                                    {{ $resturants->links() }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="new-tab-pane" role="tabpanel" aria-labelledby="new-tab" tabindex="0">
                    <div class="text-center form-group">
                        <label>بحث حسب البريد الالكتروني او الاسم باللغة العربية : </label>
                        <input type="text" class="form-controller" id="search_new" name="search"></input>
                    </div>
                    <div class="table-responsive" id="t2">

                        <table id="myTable2" class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col"></th>
                                <th scope="col">اسم المطعم</th>
                                <th scope="col">تاريخ الانضمام</th>
                                <th class="text-center" scope="col">الحالة</th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $counter++;?>
                            @foreach($join_req as $res)
{{--                                @if($res->status =='1' )--}}
                                    <tr>
                                        <td>
                                            <p class="text-center">{{$counter}}</p>
                                            <span class="text-success"></span>
                                            <?php $counter++;?>
                                        </td>
                                        <td>
                                            <div class="media">
                                                <div class="avatar me-2">
                                                    <img alt="avatar" src="/{{$res->logo}}" class="rounded-circle" />
                                                </div>
                                                <div class="media-body align-self-center">
                                                    <h6 class="mb-0">{{$res->translate('ar')->name}}</h6>
                                                    <span>{{$res->email}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{$res->created_at->toDateString()}}</p>
                                            <span class="text-success"></span>
                                        </td>
                                        <td class="text-center">
                            <span class="badge badge-light-success">
                                    بانتظار القبول
                            </span>
                                        </td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="{{route('admin_panel.resturants.show',$res->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                </a>
                                                <a href="{{route('admin_panel.resturants.edit',$res->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                </a>
                                                <a href="admin_panel/resturant_del/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
{{--                                @endif--}}
                            @endforeach

                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">

                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                                    {{ $join_req->links() }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="edit-tab-pane" role="tabpanel" aria-labelledby="edit-tab" tabindex="0">
                    <div class="text-center form-group">
                        <label>بحث حسب البريد الالكتروني او الاسم باللغة العربية : </label>
                        <input type="text" class="form-controller" id="search_edit" name="search"></input>
                    </div>
                    <div class="table-responsive" id="t3">

                        <table id="myTable3" class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col"></th>
                                <th scope="col">اسم المطعم</th>
                                <th scope="col">تاريخ الانضمام</th>
                                <th class="text-center" scope="col">الحالة</th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $counter++;?>
                            @foreach($change_req as $res)
{{--                                @if($res->status =='2')--}}
                                    <tr>
                                        <td>
                                            <p class="text-center">{{$counter}}</p>
                                            <span class="text-success"></span>
                                            <?php $counter++;?>
                                        </td>
                                        <td>
                                            <div class="media">
                                                <div class="avatar me-2">
                                                    <img alt="avatar" src="/{{$res->logo}}" class="rounded-circle" />
                                                </div>
                                                <div class="media-body align-self-center">
                                                    <h6 class="mb-0">{{$res->translate('ar')->name}}</h6>
                                                    <span>{{$res->email}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{$res->created_at->toDateString()}}</p>
                                            <span class="text-success"></span>
                                        </td>
                                        <td class="text-center">
                                <span class="badge badge-light-success">
                                        بانتظار قبول التعديلات
                                </span>
                                        </td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="{{route('admin_panel.resturants.show',$res->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                </a>
                                                <a href="{{route('admin_panel.resturants.edit',$res->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                </a>
                                                <a href="admin_panel/resturant_del/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>
                                                <a href="/admin_panel/reject/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="رفض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                         width="100" height="100"
                                                         viewBox="0 0 50 50">
                                                        <path d="M 25 2 C 12.309534 2 2 12.309534 2 25 C 2 37.690466 12.309534 48 25 48 C 37.690466 48 48 37.690466 48 25 C 48 12.309534 37.690466 2 25 2 z M 25 4 C 36.609534 4 46 13.390466 46 25 C 46 36.609534 36.609534 46 25 46 C 13.390466 46 4 36.609534 4 25 C 4 13.390466 13.390466 4 25 4 z M 32.990234 15.986328 A 1.0001 1.0001 0 0 0 32.292969 16.292969 L 25 23.585938 L 17.707031 16.292969 A 1.0001 1.0001 0 0 0 16.990234 15.990234 A 1.0001 1.0001 0 0 0 16.292969 17.707031 L 23.585938 25 L 16.292969 32.292969 A 1.0001 1.0001 0 1 0 17.707031 33.707031 L 25 26.414062 L 32.292969 33.707031 A 1.0001 1.0001 0 1 0 33.707031 32.292969 L 26.414062 25 L 33.707031 17.707031 A 1.0001 1.0001 0 0 0 32.990234 15.986328 z"></path>
                                                    </svg>
                                                </a>
                                                <a href="/admin_panel/accept/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="قبول">
                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                         width="100" height="100"
                                                         viewBox="0 0 128 128">
                                                        <path d="M 64 6 C 32 6 6 32 6 64 C 6 96 32 122 64 122 C 96 122 122 96 122 64 C 122 32 96 6 64 6 z M 64 12 C 92.7 12 116 35.3 116 64 C 116 92.7 92.7 116 64 116 C 35.3 116 12 92.7 12 64 C 12 35.3 35.3 12 64 12 z M 85.037109 48.949219 C 84.274609 48.974219 83.500391 49.300391 82.900391 49.900391 L 62 71.599609 L 51.099609 59.900391 C 49.999609 58.700391 48.100391 58.599219 46.900391 59.699219 C 45.700391 60.799219 45.599219 62.700391 46.699219 63.900391 L 59.800781 78 C 60.400781 78.6 61.1 79 62 79 C 62.8 79 63.599219 78.699609 64.199219 78.099609 L 87.199219 54 C 88.299219 52.8 88.299609 50.900781 87.099609 49.800781 C 86.549609 49.200781 85.799609 48.924219 85.037109 48.949219 z"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

{{--                                @endif--}}
                            @endforeach




                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">

                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                                    {{ $change_req->links() }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="accepted-tab-pane" role="tabpanel" aria-labelledby="accepted-tab" tabindex="0">
                    <div class="text-center form-group">
                        <label>بحث حسب البريد الالكتروني او الاسم باللغة العربية : </label>
                        <input type="text" class="form-controller" id="search_accepted" name="search"></input>
                    </div>
                    <div class="table-responsive" id="t4">

                        <table id="myTable4" class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col"></th>
                                <th scope="col">اسم المطعم</th>
                                <th scope="col">تاريخ الانضمام</th>
                                <th class="text-center" scope="col">الحالة</th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $counter++;?>
                            @foreach($accepted as $res)
{{--                                @if($res->status =='3')--}}
                                    <tr>
                                        <td>
                                            <p class="text-center">{{$counter}}</p>
                                            <span class="text-success"></span>
                                            <?php $counter++;?>
                                        </td>
                                        <td>
                                            <div class="media">
                                                <div class="avatar me-2">
                                                    <img alt="avatar" src="/{{$res->logo}}" class="rounded-circle" />
                                                </div>
                                                <div class="media-body align-self-center">
                                                    <h6 class="mb-0">{{$res->translate('ar')->name}}</h6>
                                                    <span>{{$res->email}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{$res->created_at->toDateString()}}</p>
                                            <span class="text-success"></span>
                                        </td>
                                        <td class="text-center">
                                <span class="badge badge-light-success">
                                        مقبول
                                </span>
                                        </td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="{{route('admin_panel.resturants.show',$res->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                </a>
                                                <a href="{{route('admin_panel.resturants.edit',$res->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                </a>
                                                <a href="admin_panel/resturant_del/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

{{--                                @endif--}}
                            @endforeach


                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">

                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                                    {{ $accepted->links() }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="rejected-tab-pane" role="tabpanel" aria-labelledby="rejected-tab" tabindex="0">
                    <div class="text-center form-group">
                        <label>بحث حسب البريد الالكتروني او الاسم باللغة العربية : </label>
                        <input type="text" class="form-controller" id="search_rejected" name="search"></input>
                    </div>
                    <div class="table-responsive" id="t5">

                        <table id="myTable5" class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col"></th>
                                <th scope="col">اسم المطعم</th>
                                <th scope="col">تاريخ الانضمام</th>
                                <th class="text-center" scope="col">الحالة</th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $counter++;?>
                            @foreach($rejected as $res)
{{--                                @if($res->status =='4')--}}
                                    <tr>
                                        <td>
                                            <p class="text-center">{{$counter}}</p>
                                            <span class="text-success"></span>
                                            <?php $counter++;?>
                                        </td>
                                        <td>
                                            <div class="media">
                                                <div class="avatar me-2">
                                                    <img alt="avatar" src="/{{$res->logo}}" class="rounded-circle" />
                                                </div>
                                                <div class="media-body align-self-center">
                                                    <h6 class="mb-0">{{$res->translate('ar')->name}}</h6>
                                                    <span>{{$res->email}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{$res->created_at->toDateString()}}</p>
                                            <span class="text-success"></span>
                                        </td>
                                        <td class="text-center">
                                <span class="badge badge-light-success">
                                        مرفوض
                                </span>
                                        </td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="{{route('admin_panel.resturants.show',$res->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                </a>
                                                <a href="{{route('admin_panel.resturants.edit',$res->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                </a>
                                                <a href="admin_panel/resturant_del/{{$res->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

{{--                                @endif--}}
                            @endforeach


                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">

                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                                    {{ $rejected->links() }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->
        <script type="text/javascript">
            $('#search_all').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    type : 'get',
                    url : '{{URL::to('/admin_panel/search_resturant/0')}}',
                    data:{'search':$value},
                    success:function(data){
                        $('#t1').html(data);
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $('#search_new').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    type : 'get',
                    url : '{{URL::to('/admin_panel/search_resturant/1')}}',
                    data:{'search':$value},
                    success:function(data){
                        $('#t2').html(data);
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $('#search_edit').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    type : 'get',
                    url : '{{URL::to('/admin_panel/search_resturant/2')}}',
                    data:{'search':$value},
                    success:function(data){
                        $('#t3').html(data);
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $('#search_accepted').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    type : 'get',
                    url : '{{URL::to('/admin_panel/search_resturant/3')}}',
                    data:{'search':$value},
                    success:function(data){
                        $('#t4').html(data);
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $('#search_rejected').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    type : 'get',
                    url : '{{URL::to('/admin_panel/search_resturant/4')}}',
                    data:{'search':$value},
                    success:function(data){
                        $('#t5').html(data);
                    }
                });
            })
        </script>

        <script type="text/javascript">
            $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
        </script>
        <!--
        <script>

            addPagerToTables('#myTable1', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector('tr').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector('.nav')
                        .insertAdjacentHTML(
                            'beforeend',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll('.nav a'))
                    navA.addEventListener(
                        'click',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll('.nav a');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add('active');
                    else
                        navAs[nix].classList.remove('active');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? 'table-row'
                                : 'none';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll('tbody tr');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll('td').length > 0);
            }
        </script>
        <script>

            addPagerToTables('#myTable2', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector('tr').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector('.nav')
                        .insertAdjacentHTML(
                            'beforeend',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll('.nav a'))
                    navA.addEventListener(
                        'click',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll('.nav a');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add('active');
                    else
                        navAs[nix].classList.remove('active');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? 'table-row'
                                : 'none';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll('tbody tr');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll('td').length > 0);
            }
        </script>
        <script>

            addPagerToTables('#myTable3', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector('tr').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector('.nav')
                        .insertAdjacentHTML(
                            'beforeend',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll('.nav a'))
                    navA.addEventListener(
                        'click',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll('.nav a');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add('active');
                    else
                        navAs[nix].classList.remove('active');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? 'table-row'
                                : 'none';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll('tbody tr');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll('td').length > 0);
            }
        </script>
        <script>

            addPagerToTables('#myTable4', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector('tr').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector('.nav')
                        .insertAdjacentHTML(
                            'beforeend',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll('.nav a'))
                    navA.addEventListener(
                        'click',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll('.nav a');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add('active');
                    else
                        navAs[nix].classList.remove('active');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? 'table-row'
                                : 'none';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll('tbody tr');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll('td').length > 0);
            }
        </script>
        <script>

            addPagerToTables('#myTable5', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector('tr').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector('.nav')
                        .insertAdjacentHTML(
                            'beforeend',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll('.nav a'))
                    navA.addEventListener(
                        'click',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll('.nav a');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add('active');
                    else
                        navAs[nix].classList.remove('active');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? 'table-row'
                                : 'none';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll('tbody tr');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll('td').length > 0);
            }
        </script>
        -->
    </div>



@endsection

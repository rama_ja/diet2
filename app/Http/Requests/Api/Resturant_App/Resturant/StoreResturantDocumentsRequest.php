<?php

namespace App\Http\Requests\Api\Resturant_App\Resturant;

use Illuminate\Foundation\Http\FormRequest;

class StoreResturantDocumentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // The 3 Satage Data
            'commercial_record'               => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'municipal_licence'               => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}

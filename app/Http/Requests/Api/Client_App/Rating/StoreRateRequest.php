<?php

namespace App\Http\Requests\Api\Client_APP\Rating;

use Illuminate\Foundation\Http\FormRequest;

class StoreRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'representative_id'        => 'required',
            'representative_evaluation'=> 'required',
            'order_id'                 => 'required',
            'order_evaluation'         => 'required',
            'resturant_id'             => 'required',
            'resturant_evaluation'     => 'required',
            'client_experience'        => 'required|string'
        ];
    }
}


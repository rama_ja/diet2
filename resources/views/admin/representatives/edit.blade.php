@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">المندوبين </li>
                                    <li class="breadcrumb-item active"> تعديل المندوب </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">تعديل معلومات المندوب</h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">
                                <form class=" " method="post" action="{{route('admin_panel.representatives.update',$representative->id)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif
                                   <div class="row">
                                       <div class="row">

                                           <div class="col-md-6">
                                               <label for="inputAddress" class="form-label"> الصورة الشخصية </label><br>
                                               <img alt="avatar" src="/{{$representative->personal_image}}"  width="200" height="200"/>
                                               <br><br>
                                               <input type="file" class="form-control"  placeholder="1234 Main St" name="personal_image">
                                           </div>

                                           <div class="col-md-6">
                                               <label for="inputAddress" class="form-label"> صورة الشهادة </label><br>
                                               <img alt="avatar" src="/{{$representative->license_image}}"  width="300" height="200"/>
                                               <br><br>
                                               <input type="file" class="form-control"  placeholder="1234 Main St" name="license_image">
                                           </div>
                                           <div class="col-md-6">
                                               <label for="inputEmail4" class="form-label">البريد الالكتروني</label>
                                               <input type="email" class="form-control"  value="{{$representative->email}}" name="email">
                                           </div>
                                           <div class="col-md-6">
                                               <label for="inputEmail4" class="form-label">رقم الجوال</label>
                                               <input type="tel" class="form-control"  value="{{$representative->mobile}}" name="mobile">
                                           </div>
                                           <div class="col-md-6">
                                               <label for="inputEmail4" class="form-label">الاسم </label>
                                               <input type="text" class="form-control"  value="{{$representative->name}}" name="name">
                                           </div>

                                           <div class="col-md-6">
                                               <label  for="inputEmail4" class="form-label"> نوع الحافلة </label>
                                               <input type="text" class="form-control"  value="{{$representative->vehicle_type}}" name="vehicle_type">
                                           </div>

                                           <div class="col-md-6">
                                               <label for="inputState" class="form-label">المدينة</label>
                                               <select class="form-select" name="city_id">
                                                   <option selected value="{{$representative->city->id}}">{{$representative->city->translate('ar')->name}} </option>
                                                   @foreach($cities as $city)
                                                       @if($city->id != $representative->city->id)
                                                           <option value="{{$city->id}}">{{$city->translate('ar')->name}}</option>
                                                       @endif
                                                   @endforeach
                                               </select>
                                           </div>
                                           <div class="col-md-6">
                                               <label for="inputState" class="form-label"> المطعم التابع له </label>
                                               <select class="form-select" name="resturant_id">
                                                   @if($representative->resturant)
                                                       <option selected value="{{$representative->resturant->id}}">{{$representative->resturant->translate('ar')->name}} </option>
                                                       @foreach($resturants as $resturant)
                                                           @if($resturant->id != $representative->resturant->id)
                                                               <option value="{{$resturant->id}}">{{$resturant->translate('ar')->name}}</option>
                                                           @endif
                                                       @endforeach
                                                   @else
                                                       <option selected value="null">لايوجد </option>
                                                       @foreach($resturants as $resturant)
                                                           <option value="{{$resturant->id}}">{{$resturant->translate('ar')->name}}</option>
                                                       @endforeach
                                                   @endif
                                               </select>
                                           </div>


                                           <div class="col-md-6">
                                               <label  for="inputEmail4" class="form-label"> رقم الحافلة </label>
                                               <input type="text" class="form-control"  value="{{$representative->vehicle_number}}" name="vehicle_number">
                                           </div>

                                           <div class="col-md-6">
                                               <label for="inputAddress" class="form-label"> الترخيص </label><br>
                                               <img alt="avatar" src="/{{$representative->form_image}}"  width="300" height="200"/>
                                               <br><br>
                                               <input type="file" class="form-control"  placeholder="1234 Main St" name="form_image">
                                           </div>

                                           <div class="row col-md-6">
                                               <label for="inputEmail4" class="form-label"> تفاصيل الحافلة </label>
                                               <div class="col-md-4">
                                                   <label for="inputState" class="form-label"> الماركة </label>
                                                   <input type="text" class="form-control"  value="{{$representative->vehicle_brand}}" name="vehicle_brand">
                                               </div>
                                               <div class="col-md-4">
                                                   <label for="inputState" class="form-label"> النوع </label>
                                                   <input type="text" class="form-control"  value="{{$representative->type}}" name="type">
                                               </div>
                                               <div class="col-md-4">
                                                   <label for="inputState" class="form-label"> الموديل </label>
                                                   <input type="text" class="form-control"  value="{{$representative->vehicle_model}}" name="vehicle_model">
                                               </div>
                                           </div>

                                           <div class="col-12">
                                               <div class="row">
                                                   <div id="splider_MultipleSlider" class="col-lg-12 col-md-12 layout-spacing">
                                                       <div class="statbox widget box box-shadow">
                                                           <div class="widget-header">
                                                               <div class="row">
                                                                   <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                       <h4>صور الحافلة </h4>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="widget-content widget-content-area">
                                                               <div class="position-relative">
                                                                   <div class="container" style="max-width: 700px">
                                                                       <div class="splide-multiple">
                                                                           <div class="splide__track">
                                                                               <ul class="splide__list">
                                                                                   @foreach($representative->images as $img)
                                                                                       <li class="splide__slide">
                                                                                           <img alt="slider-image" class="img-fluid" src="/{{$img->image}}" width="100" height="100">
                                                                                       </li>
                                                                                   @endforeach

                                                                               </ul>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </div>


                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>


                                           <div class="col-md-6">
                                               <label for="inputState" class="form-label">الحالة</label>
                                               <select class="form-select" name="status">
                                                   @if($representative->status ==1)
                                                       <option selected value="1"> مقبول </option>
                                                       <option value="2">غير مقبول </option>
                                                   @elseif($representative->status ==2)
                                                       <option value="1"> مقبول </option>
                                                       <option selected value="2">غير مقبول </option>
                                                   @endif
                                               </select>
                                           </div>

                                           <div class="text-center">
                                               <button type="submit" class="btn btn-primary">تعديل</button>
                                           </div>

                                       </div>
                                   </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection

<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
//['delivery_cost', 'kilometer_cost','primitive_distance','mobile_contact']
    public function index()
    {
        $admin=auth('admin')->user();
        $setting=Setting::get()->first();
        return view('admin.settings.edit',['admin'=>$admin,'setting'=>$setting]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $setting=Setting::get()->last();

        $setting->update([
            'delivery_cost'                   => $request->delivery_cost,
            'kilometer_cost'                  => $request->kilometer_cost,
            'primitive_distance'              => $request->primitive_distance,
            'mobile_contact'                  => $request->mobile_contact
        ]);

        return redirect(route('admin_panel.settings.index'));
    }


}


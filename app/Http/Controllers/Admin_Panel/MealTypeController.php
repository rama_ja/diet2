<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Models\MealType;
use Illuminate\Http\Request;

class MealTypeController extends Controller
{

    public function index(Request $request)
    {
        $admin=auth('admin')->user();
        $meal_types = MealType::orderBy('created_at', 'DESC')->paginate(15);
        return view('admin.meal_type.index',['meal_types'=>$meal_types,'admin'=>$admin]);
    }

    public function store(Request $request)
    {
      $type=MealType::create([
          'ar'  => [
              'name'        => $request->name_ar,
          ],
          'en'  => [
              'name'        => $request->name_en,
          ]
      ]);

        return redirect(route('admin_panel.meal_types.index'));

    }
    public function search(Request $request)
    {
        if($request->ajax())
        {
            $output="";

            $status=$request->search;
            $meal_types=MealType::where('status',$status )->orderBy('created_at', 'DESC')->get();

            if($status==3){
                $meal_types=MealType::orderBy('created_at', 'DESC')->get();

            }
            if($meal_types)
            {$counter=1;
            $output.='      
        <table id="myTable1" class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center" scope="col"></th>
                    <th scope="col">التصنيف باللغة العربية</th>
                    <th scope="col">التصنيف باللغة الأجنبية</th>
                    <th scope="col">الحالة </th>
                    <th scope="col">تاريخ الإضافة</th>
                    <th class="text-center" scope="col"></th>
                </tr>
                </thead>
                <tbody>';
                foreach ($meal_types as $key => $meal_type) {
                    $output.='<tr>'.
                        '      <td>
                            <p class="text-center">'.$counter.'</p>
                            <span class="text-success"></span>
                        </td>                    
                        <td>
                            <p class="mb-0">'.$meal_type->translate('ar')->name.'</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <p class="mb-0">'.$meal_type->translate('en')->name.'</p>
                            <span class="text-success"></span>
                        </td>
                         <td>';
                         if($meal_type->status==1)
                             $output.=' <p class="mb-0">مقبول</p>';
                         else
                             $output.=' <p class="mb-0">غير مقبول</p>';

                          $output.='  <span class="text-success"></span>
                        </td>
                         <td>
                            <p class="mb-0">'.$meal_type->created_at->toDateString().'</p>
                            <span class="text-success"></span>
                        </td>
                        <td class="text-center">
                            <div class="action-btns">                             
                                 <a href="'.route('admin_panel.meal_types.edit',$meal_type->id).'" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                </a>
                                <a href="/admin_panel/meal_type_del/'.$meal_type->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </a>
                                <a href="/admin_panel/reject_meal_type/'.$meal_type->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="رفض">
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         width="100" height="100"
                                         viewBox="0 0 50 50">
                                        <path d="M 25 2 C 12.309534 2 2 12.309534 2 25 C 2 37.690466 12.309534 48 25 48 C 37.690466 48 48 37.690466 48 25 C 48 12.309534 37.690466 2 25 2 z M 25 4 C 36.609534 4 46 13.390466 46 25 C 46 36.609534 36.609534 46 25 46 C 13.390466 46 4 36.609534 4 25 C 4 13.390466 13.390466 4 25 4 z M 32.990234 15.986328 A 1.0001 1.0001 0 0 0 32.292969 16.292969 L 25 23.585938 L 17.707031 16.292969 A 1.0001 1.0001 0 0 0 16.990234 15.990234 A 1.0001 1.0001 0 0 0 16.292969 17.707031 L 23.585938 25 L 16.292969 32.292969 A 1.0001 1.0001 0 1 0 17.707031 33.707031 L 25 26.414062 L 32.292969 33.707031 A 1.0001 1.0001 0 1 0 33.707031 32.292969 L 26.414062 25 L 33.707031 17.707031 A 1.0001 1.0001 0 0 0 32.990234 15.986328 z"></path>
                                    </svg>
                                </a>
                                <a href="/admin_panel/accept_meal_type/'.$meal_type->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="قبول">
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         width="100" height="100"
                                         viewBox="0 0 128 128">
                                        <path d="M 64 6 C 32 6 6 32 6 64 C 6 96 32 122 64 122 C 96 122 122 96 122 64 C 122 32 96 6 64 6 z M 64 12 C 92.7 12 116 35.3 116 64 C 116 92.7 92.7 116 64 116 C 35.3 116 12 92.7 12 64 C 12 35.3 35.3 12 64 12 z M 85.037109 48.949219 C 84.274609 48.974219 83.500391 49.300391 82.900391 49.900391 L 62 71.599609 L 51.099609 59.900391 C 49.999609 58.700391 48.100391 58.599219 46.900391 59.699219 C 45.700391 60.799219 45.599219 62.700391 46.699219 63.900391 L 59.800781 78 C 60.400781 78.6 61.1 79 62 79 C 62.8 79 63.599219 78.699609 64.199219 78.099609 L 87.199219 54 C 88.299219 52.8 88.299609 50.900781 87.099609 49.800781 C 86.549609 49.200781 85.799609 48.924219 85.037109 48.949219 z"></path>
                                    </svg>
                                </a>
                            </div>
                        </td>'.
                        '</tr>';
                    $counter++;
                }$output.='      </tbody>

            </table>
            <a class="btn btn-primary mb-2 me-4" href="{{route(\'admin_panel.meal_types.create\')}}">إضافةتصنيف </a>

            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                    </div>
                </div>
                <div class="col-sm-12 col-md-7">

                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                    

                    </div>
                </div>
            </div>
             <script>

            addPagerToTables(\'#myTable1\', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector(\'tr\').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector(\'.nav\')
                        .insertAdjacentHTML(
                            \'beforeend\',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll(\'.nav a\'))
                    navA.addEventListener(
                        \'click\',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll(\'.nav a\');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add(\'active\');
                    else
                        navAs[nix].classList.remove(\'active\');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? \'table-row\'
                                : \'none\';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll(\'tbody tr\');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll(\'td\').length > 0);
            }
        </script>';
                return Response($output);
            }

        }
    }

    public function create(Request $request)
    {
        $admin=auth('admin')->user();

      return view('admin.meal_type.create',['admin'=>$admin]);
    }

     public function edit(Request $request,$id)
    {
        $meal_type=MealType::find($id);
        $admin=auth('admin')->user();
      return view('admin.meal_type.edit',['admin'=>$admin,'meal_type'=>$meal_type]);
    }
    public function update(Request $request,$id)
    {
        $type=MealType::find($id);

        $type->update([
            'ar'  => [
                'name'        => $request->name_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
            ],
            'status' => $request->status
        ]);

        return redirect(route('admin_panel.meal_types.index'));
    }

    public function reject($id)
    {
        $type=MealType::find($id);
        $type->update(['status'=>'2']);
        return redirect()->back();
    }

    public function accept($id)
    {
        $type=MealType::find($id);
        $type->update(['status'=>'1']);
        return redirect()->back();
    }

    public function destroy($id)
    {
        $type=MealType::find($id);
        $type->delete();
        return redirect()->back();
    }
}

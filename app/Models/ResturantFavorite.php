<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResturantFavorite extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['resturant_id', 'client_id'];
}

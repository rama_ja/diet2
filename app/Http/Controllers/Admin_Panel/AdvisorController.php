<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Http\Traits\SaveImageTrait;
use App\Models\Admin;
use App\Models\Advisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdvisorController extends Controller
{
    use SaveImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin=auth('admin')->user();
        $advisors=Advisor::orderBy('created_at', 'DESC')->paginate(15);
        return view('admin.advisors.index',['admin'=>$admin,'advisors'=>$advisors]);
    }

    public function search(Request $request)
    {
        if($request->ajax())
        {
            $output="";
            $advisors=Advisor::where('name','LIKE','%'.$request->search."%")->orderBy('created_at', 'DESC')->get();
            if($advisors)
            {$counter=1;
            $output.='  <table id="myTable1" class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center" scope="col"></th>
                    <th scope="col">اسم المستشار</th>
                    <th scope="col">تاريخ الانضمام</th>
                    <th class="text-center" scope="col"></th>
                </tr>
                </thead>
                <tbody>';
                foreach ($advisors as $key => $advisor) {
                    $output.='<tr>
                        <td>
                            <p class="text-center">'.$counter.'</p>
                            <span class="text-success"></span>
                        </td>
                        <td>
                            <div class="media">
                                <div class="avatar me-2">
                                    <img alt="avatar" src="/'.$advisor->personal_image.'" class="rounded-circle" />
                                </div>
                                <div class="media-body align-self-center">
                                    <h6 class="mb-0">'.$advisor->name.'</h6>
                                 
                                </div>
                            </div>
                        </td>
                        <td>
                            <p class="mb-0">'.$advisor->created_at->toDateString().'</p>
                            <span class="text-success"></span>
                        </td>
                        <td class="text-center">
                            <div class="action-btns">
                                <a href="'.route('admin_panel.advisors.show',$advisor->id).'" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </a>  
                                <a href="'.route('admin_panel.advisors.edit',$advisor->id).'" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                </a>                 
                                <a href="/admin_panel/client_del/'.$advisor->id.'" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </a>
                            </div>
                        </td>'.
                        '</tr>';
                    $counter++;
                }
                $output.='           </tbody>

            </table>
            <a class="btn btn-primary mb-2 me-4" href="{{route(\'admin_panel.advisors.create\')}}">إضافة مستشار </a>

            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                    </div>
                </div>
                <div class="col-sm-12 col-md-7">

                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                     

                    </div>
                </div>
            </div>

   <script>

            addPagerToTables(\'#myTable1\', 4);

            function addPagerToTables(tables, rowsPerPage = 10) {

                tables =
                    typeof tables == "string"
                        ? document.querySelectorAll(tables)
                        : tables;

                for (let table of tables)
                    addPagerToTable(table, rowsPerPage);

            }

            function addPagerToTable(table, rowsPerPage = 10) {

                let tBodyRows = getBodyRows(table);
                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);

                let colCount =
                    [].slice.call(
                        table.querySelector(\'tr\').cells
                    )
                        .reduce((a,b) => a + parseInt(b.colSpan), 0);

                table
                    .createTFoot()
                    .insertRow()
                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;

                if(numPages == 1)
                    return;

                for(i = 0;i < numPages;i++) {

                    let pageNum = i + 1;

                    table.querySelector(\'.nav\')
                        .insertAdjacentHTML(
                            \'beforeend\',
                            `<a href="#" rel="${i}">${pageNum}</a> `
                        );

                }

                changeToPage(table, 1, rowsPerPage);

                for (let navA of table.querySelectorAll(\'.nav a\'))
                    navA.addEventListener(
                        \'click\',
                        e => changeToPage(
                            table,
                            parseInt(e.target.innerHTML),
                            rowsPerPage
                        )
                    );

            }

            function changeToPage(table, page, rowsPerPage) {

                let startItem = (page - 1) * rowsPerPage;
                let endItem = startItem + rowsPerPage;
                let navAs = table.querySelectorAll(\'.nav a\');
                let tBodyRows = getBodyRows(table);

                for (let nix = 0; nix < navAs.length; nix++) {

                    if (nix == page - 1)
                        navAs[nix].classList.add(\'active\');
                    else
                        navAs[nix].classList.remove(\'active\');

                    for (let trix = 0; trix < tBodyRows.length; trix++)
                        tBodyRows[trix].style.display =
                            (trix >= startItem && trix < endItem)
                                ? \'table-row\'
                                : \'none\';

                }

            }

            // tbody might still capture header rows if
            // if a thead was not created explicitly.
            // This filters those rows out.
            function getBodyRows(table) {
                let initial = table.querySelectorAll(\'tbody tr\');
                return Array.from(initial)
                    .filter(row => row.querySelectorAll(\'td\').length > 0);
            }
        </script>';
                return Response($output);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin=auth('admin')->user();
        return view('admin.advisors.create',['admin'=>$admin]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin=auth('admin')->user();
        try {
            $advisor = Advisor::create([
                'name' => $request->name,
                'mobile' => $request->mobile,
                'description' => $request->description
            ]);
            if ($request->file('personal_image')) {
                $advisor->update([
                    'personal_image' => $this->saveImage($request->file('personal_image'), 'Images/Advisors', 400, 400),
                ]);
            }
        }catch (\Exception $e) { // It's actually a QueryException but this works too
            if ($e->getCode() == 23000) {
                return  redirect()->back()->withErrors(['msg' => 'رقم الجوال تم اخذه مسبقا  ']);
            }
        }
        return redirect(route('admin_panel.advisors.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin=auth('admin')->user();
        $advisor=Advisor::find($id);
        return view('admin.advisors.item',['admin'=>$admin,'advisor'=>$advisor]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin=auth('admin')->user();
        $advisor=Advisor::find($id);
        return view('admin.advisors.edit',['admin'=>$admin,'advisor'=>$advisor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advisor=Advisor::find($id);
        try {
            $advisor->update([
                'name' => $request->name,
                'mobile' => $request->mobile,
                'description' => $request->description
            ]);

            if($request->file('personal_image')){
                if( preg_match("/\b(" . "avatar" . ")\b/i", $advisor->personal_image) ){
                    $advisor->update([
                        'personal_image'  => $this->saveImage($request->file('personal_image'), 'Images/Advisors', 400, 400),
                    ]);
                }else{
                    $advisor->update([
                        'personal_image'         => $this->updateImage($advisor, $request, 'personal_image', 'Images/Advisors', 400, 400)
                    ]);
                }

            }

        } catch (\Exception $e) { // It's actually a QueryException but this works too
            if ($e->getCode() == 23000) {
                return  redirect()->back()->withErrors(['msg' => 'رقم الجوال تم اخذه مسبقا  ']);
            }
        }

        return redirect(route('admin_panel.advisors.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advisor=Advisor::find($id);
        if( !preg_match("/\b(" . "avatar" . ")\b/i", $advisor->personal_image) ) {
            (File::exists($advisor->personal_image)) ? File::delete($advisor->personal_image) : Null;
        }
        $advisor->delete();

        return redirect()->back();
    }
}

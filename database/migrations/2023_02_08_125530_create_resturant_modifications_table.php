<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResturantModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resturant_modifications', function (Blueprint $table) {
            $table->id();
            // The data of the 1 stage
            $table->foreignId('resturant_id')->constrained('resturants')->onDelete('cascade');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
//            $table->string('password');

            // The data of the 2 stage
            $table->foreignId('city_id')->nullable()->constrained('cities')->onDelete('cascade');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('logo')->nullable();
            $table->string('main_image')->nullable();

            // The data of the 3 stage
            $table->string('commercial_record')->nullable();
            $table->string('municipal_licence')->nullable();

            // The data of the 4 stage will store it in another table
            $table->boolean('status')->comment('0 Data not completed yet, 1 Waiting for approval to join, 2 Waiting for the modifications to be accepted, 3 acceptable, 4 rejected')->default('0');

            // Setting data
            $table->string('language', 4)->comment('ar:Arabic , en:English');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resturant_modifications');
    }
}

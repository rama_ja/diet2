<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_APP\Rating\StoreRateRequest;
use App\Http\Resources\Api\Client_APP\Rating\RatingResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Rating;
use App\Models\Resturant;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    use  ApiResponseTrait;

    public function addEvaluation(StoreRateRequest $request){
        $client = $request->user('client_api');
        $evaluation=Rating::create([
            'client_id'                    => $client->id,
            'resturant_id'                 => $request->resturant_id,
            'resturant_evaluation'         => $request->resturant_evaluation,
            'client_experience'            => $request->client_experience,
            'representative_id'            => $request->representative_id,
            'representative_evaluation'    => $request->representative_evaluation,
            'order_id'                     => $request->order_id,
            'order_evaluation'             => $request->order_evaluation
        ]);
        return $this->apiResponse(new RatingResource($evaluation), 'The evaluation has been added successfully ', 200);
    }

    public function getRates(Request $request){

        $client = $request->user('client_api');
        $rates=Rating::where('client_id',$client->id)->orderBy('created_at', 'DESC')->paginate(20);

        return $this->apiResponse(RatingResource::collection($rates), 'Client rates ', 200);
    }

}

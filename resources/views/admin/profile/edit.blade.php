@extends('admin.dashboard')

@section('content')


    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="middle-content container-xxl p-0">

                <!--  BEGIN BREADCRUMBS  -->
                <div class="secondary-nav">
                    <div class="breadcrumbs-container" data-page-heading="Analytics">
                        <header class="header navbar navbar-expand-sm">
                            <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                            </a>
                            <div class="d-flex breadcrumb-content">
                                <div class="page-header">

                                    <div class="page-title">
                                    </div>

                                    <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">المستخدمون </a></li>
                                            <li class="breadcrumb-item active" aria-current="page"> الملف الشخصي </li>
                                        </ol>
                                    </nav>

                                </div>
                            </div>
                        </header>
                    </div>
                </div>
                <!--  END BREADCRUMBS  -->

                <div class="row layout-spacing ">

                    <!-- Content -->
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 layout-top-spacing">
                        <div class="user-profile">
                            <div class="widget-content widget-content-area">
                                <div class="d-flex justify-content-center">
                                    <h3 class=""> تعديل الملف الشخصي </h3>
                                </div>

{{--                                <div class="text-center user-info">--}}
{{--                                    <img src="/{{$admin->personal_image}}" alt="avatar">--}}
{{--                                    <p class="">{{$admin->name}}</p>--}}
{{--                                </div>--}}
                                <div class="contacts-block list-unstyled container">

                                    <form class="text-center user-info" method="post" action="{{route('admin_panel.profile.update',$admin->id)}}" enctype="multipart/form-data" >
                                        @method('PATCH')
                                        @csrf
                                        @if ($errors->any())

                                            <div class="alert alert-danger">

                                                <ul style="list-style: none;margin:0">

                                                    @foreach ($errors->all() as $error)

                                                        <li>{{ $error }}</li>

                                                    @endforeach

                                                </ul>

                                            </div>

                                        @endif
                                        <div class="contacts-block list-unstyled">
                                            <img alt="avatar" src="/{{$admin->personal_image}}"  width="200" height="200"/>
                                            <br><br>
                                            <input type="file" class="form-control"  placeholder="1234 Main St" name="personal_image">
                                        </div>

                                        <div class="">
                                            <label for="inputEmail4" class="form-label">الاسم </label>
                                            <input type="text" class="form-control"  value="{{$admin->name}}" name="name">
                                        </div>

                                        <div class="">
                                            <label for="inputEmail4" class="form-label">البريد الالكتروني</label>
                                            <input type="email" class="form-control"  value="{{$admin->email}}" name="email">
                                        </div>



                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">تعديل</button>
                                        </div>


                                    </form>

{{--                                    <div class="">--}}
{{--                                        <ul class="contacts-block list-unstyled">--}}
{{--                                            <!----}}
{{--                                             <li class="contacts-block__item">--}}
{{--                                                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-coffee me-3"><path d="M18 8h1a4 4 0 0 1 0 8h-1"></path><path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path><line x1="6" y1="1" x2="6" y2="4"></line><line x1="10" y1="1" x2="10" y2="4"></line><line x1="14" y1="1" x2="14" y2="4"></line></svg> web developer--}}
{{--                                             </li>--}}
{{--                                             <li class="contacts-block__item">--}}
{{--                                                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin me-3"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>New York, USA--}}
{{--                                             </li>--}}
{{--                                             <li class="contacts-block__item">--}}
{{--                                                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone me-3"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg> +1 (530) 555-12121--}}
{{--                                             </li>--}}
{{--                                             -->--}}
{{--                                            <li class="contacts-block__item">--}}
{{--                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar me-3"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>{{$admin->created_at}}--}}
{{--                                            </li>--}}

{{--                                            <li class="contacts-block__item">--}}
{{--                                                <a href="mailto:example@mail.com"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail me-3"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>{{$admin->email}}</a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}

{{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>


@endsection

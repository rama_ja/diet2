<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Package\StoreRateRequest;
use App\Http\Resources\Api\Client_App\Meal\MealRatingResource;
use App\Http\Resources\Api\Client_APP\Rating\RatingResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Order;
use App\Models\Rating;
use App\Models\Representative;
use Illuminate\Http\Request;

class RepresentativeRatingController extends Controller
{
    use ApiResponseTrait;


    public function getRates($id){

        $representative=Representative::find($id);
        $rates=Rating::where('representative_id',$representative->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(RatingResource::collection($rates), 'Representative rates ', 200);
    }

}

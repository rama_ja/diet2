@extends('admin.dashboard')

@section('content')
    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> العملاء</li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="text-center form-group">
            <label>بحث حسب البريد الالكتروني او الاسم : </label>
            <input type="text" class="form-controller" id="search" name="search"></input>
        </div>
        <div class="table-responsive" id="t1">

            <table id="myTable1" class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center" scope="col"></th>
                    <th scope="col">اسم العميل</th>
                    <th scope="col">تاريخ الانضمام</th>
                    <th class="text-center" scope="col"></th>
                </tr>
                </thead>
                <tbody>

                <?php $counter=1;?>
                @foreach($clients as $client)

                    <tr>
                        <td>
                            <p class="text-center">{{$counter}}</p>
                            <span class="text-success"></span>
                            <?php $counter++;?>
                        </td>
                        <td>
                            <div class="media">
                                <div class="avatar me-2">
                                    <img alt="avatar" src="/{{$client->personal_image}}" class="rounded-circle" />
                                </div>
                                <div class="media-body align-self-center">
                                    <h6 class="mb-0">{{$client->name}}</h6>
                                    <span>{{$client->email}}</span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <p class="mb-0">{{$client->created_at->toDateString()}}</p>
                            <span class="text-success"></span>
                        </td>
                        <td class="text-center">
                            <div class="action-btns">
                                <a href="{{route('admin_panel.clients.show',$client->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="عرض">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </a>
                                <a href="{{route('admin_panel.clients.edit',$client->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                </a>
                                <a href="/admin_panel/client_orders/{{$client->id}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="الطلبات">
                                    <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>
                                </a>
                                <a href="/admin_panel/client_rates/{{$client->id}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="التقييمات">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-award" viewBox="0 0 16 16"><path d="M9.669.864 8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193.684 1.365 1.086 1.072L12.387 6l.248 1.506-1.086 1.072-.684 1.365-1.51.229L8 10.874l-1.355-.702-1.51-.229-.684-1.365-1.086-1.072L3.614 6l-.25-1.506 1.087-1.072.684-1.365 1.51-.229L8 1.126l1.356.702 1.509.229z"/><path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"/></svg>
                                </a>
                                <a href="/admin_panel/client_del/{{$client->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="حذف">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </a>
                            </div>
                        </td>
                    </tr>

                @endforeach
                </tbody>

            </table>
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">

                    </div>
                </div>
                <div class="col-sm-12 col-md-7">

                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">

                        {{ $clients->links() }}

                    </div>
                </div>
            </div>

        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->
        <script type="text/javascript">
            $('#search').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    type : 'get',
                    url : '{{URL::to('/admin_panel/search_client')}}',
                    data:{'search':$value},
                    success:function(data){
                        $('#t1').html(data);
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
        </script>
{{--        <script>--}}

{{--            addPagerToTables('#myTable1', 4);--}}

{{--            function addPagerToTables(tables, rowsPerPage = 10) {--}}

{{--                tables =--}}
{{--                    typeof tables == "string"--}}
{{--                        ? document.querySelectorAll(tables)--}}
{{--                        : tables;--}}

{{--                for (let table of tables)--}}
{{--                    addPagerToTable(table, rowsPerPage);--}}

{{--            }--}}

{{--            function addPagerToTable(table, rowsPerPage = 10) {--}}

{{--                let tBodyRows = getBodyRows(table);--}}
{{--                let numPages = Math.ceil(tBodyRows.length/rowsPerPage);--}}

{{--                let colCount =--}}
{{--                    [].slice.call(--}}
{{--                        table.querySelector('tr').cells--}}
{{--                    )--}}
{{--                        .reduce((a,b) => a + parseInt(b.colSpan), 0);--}}

{{--                table--}}
{{--                    .createTFoot()--}}
{{--                    .insertRow()--}}
{{--                    .innerHTML = `<td colspan=${colCount}><div class="nav"></div></td>`;--}}

{{--                if(numPages == 1)--}}
{{--                    return;--}}

{{--                for(i = 0;i < numPages;i++) {--}}

{{--                    let pageNum = i + 1;--}}

{{--                    table.querySelector('.nav')--}}
{{--                        .insertAdjacentHTML(--}}
{{--                            'beforeend',--}}
{{--                            `<a href="#" rel="${i}">${pageNum}</a> `--}}
{{--                        );--}}

{{--                }--}}

{{--                changeToPage(table, 1, rowsPerPage);--}}

{{--                for (let navA of table.querySelectorAll('.nav a'))--}}
{{--                    navA.addEventListener(--}}
{{--                        'click',--}}
{{--                        e => changeToPage(--}}
{{--                            table,--}}
{{--                            parseInt(e.target.innerHTML),--}}
{{--                            rowsPerPage--}}
{{--                        )--}}
{{--                    );--}}

{{--            }--}}

{{--            function changeToPage(table, page, rowsPerPage) {--}}

{{--                let startItem = (page - 1) * rowsPerPage;--}}
{{--                let endItem = startItem + rowsPerPage;--}}
{{--                let navAs = table.querySelectorAll('.nav a');--}}
{{--                let tBodyRows = getBodyRows(table);--}}

{{--                for (let nix = 0; nix < navAs.length; nix++) {--}}

{{--                    if (nix == page - 1)--}}
{{--                        navAs[nix].classList.add('active');--}}
{{--                    else--}}
{{--                        navAs[nix].classList.remove('active');--}}

{{--                    for (let trix = 0; trix < tBodyRows.length; trix++)--}}
{{--                        tBodyRows[trix].style.display =--}}
{{--                            (trix >= startItem && trix < endItem)--}}
{{--                                ? 'table-row'--}}
{{--                                : 'none';--}}

{{--                }--}}

{{--            }--}}

{{--            // tbody might still capture header rows if--}}
{{--            // if a thead was not created explicitly.--}}
{{--            // This filters those rows out.--}}
{{--            function getBodyRows(table) {--}}
{{--                let initial = table.querySelectorAll('tbody tr');--}}
{{--                return Array.from(initial)--}}
{{--                    .filter(row => row.querySelectorAll('td').length > 0);--}}
{{--            }--}}
{{--        </script>--}}

    </div>



@endsection

<?php

use App\Http\Controllers\Admin_Panel\AuthController;
use App\Models\Admin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth:admin'])->name('dashboard');

//
//require __DIR__.'/auth.php';
//Route::get('/clear-cache', function() {
//    $exitCode = Artisan::call('cache:clear');
//    // return what you want
//});
/*
|--------------------------------------------------------------------------
| Admin Authentication
|--------------------------------------------------------------------------
*/


Route::group(['prefix' => 'admin_panel','as' => 'admin_panel.'], function () {
//    Route::post('/register', [AuthController::class, 'register']);
    Route::get('/login_view', [AuthController::class, 'loginView'])->name('login');
    Route::post('/login', [AuthController::class, 'login']);

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/', [AuthController::class, 'analys']);
        Route::resource('profile', AuthController::class);
        Route::get('/edit_password', [AuthController::class, 'editPassword']);
        Route::post('/update_password', [AuthController::class, 'updatePassword']);
        Route::get('/logout', [AuthController::class, 'logout']);

        ## Resturant Routes
        Route::resource('resturants', \App\Http\Controllers\Admin_Panel\ResturantController::class);
        Route::get('/resturant_del/{id}', [\App\Http\Controllers\Admin_Panel\ResturantController::class, 'destroy']);
        Route::get('/search_resturant/{id}',[\App\Http\Controllers\Admin_Panel\ResturantController::class,'search']);
        Route::get('/reject/{id}',[\App\Http\Controllers\Admin_Panel\ResturantController::class,'reject']);
        Route::get('/accept/{id}',[\App\Http\Controllers\Admin_Panel\ResturantController::class,'accept']);


        ## Representative Routes
        Route::resource('representatives', \App\Http\Controllers\Admin_Panel\RepresentativeController::class);
        Route::get('/representative_del/{id}', [\App\Http\Controllers\Admin_Panel\RepresentativeController::class, 'destroy']);
        Route::get('/search_representative',[\App\Http\Controllers\Admin_Panel\RepresentativeController::class,'search']);

        ## Client Routes
        Route::resource('clients', \App\Http\Controllers\Admin_Panel\ClientController::class);
        Route::get('/client_del/{id}', [\App\Http\Controllers\Admin_Panel\ClientController::class, 'destroy']);
        Route::get('/client_orders/{id}', [\App\Http\Controllers\Admin_Panel\ClientController::class, 'orders']);
        Route::get('/client_rates/{id}', [\App\Http\Controllers\Admin_Panel\ClientController::class, 'rates']);
        Route::get('/search_client',[\App\Http\Controllers\Admin_Panel\ClientController::class,'search']);

        ## Advisor Routes
        Route::resource('advisors', \App\Http\Controllers\Admin_Panel\AdvisorController::class);
        Route::get('/advisor_del/{id}', [\App\Http\Controllers\Admin_Panel\AdvisorController::class, 'destroy']);
        Route::get('/search_advisor',[\App\Http\Controllers\Admin_Panel\AdvisorController::class,'search']);

        ## Setting Routes
        Route::resource('settings', \App\Http\Controllers\Admin_Panel\SettingController::class);

        ## Coupon Routes
        Route::resource('coupons', \App\Http\Controllers\Admin_Panel\CouponController::class);
        Route::get('/coupon_del/{id}', [\App\Http\Controllers\Admin_Panel\CouponController::class, 'destroy']);
        Route::get('/search_coupon',[\App\Http\Controllers\Admin_Panel\CouponController::class,'search']);

        ## Meal Types Routes
        Route::resource('meal_types', \App\Http\Controllers\Admin_Panel\MealTypeController::class);
        Route::get('/meal_type_del/{id}', [\App\Http\Controllers\Admin_Panel\MealTypeController::class, 'destroy']);
        Route::get('/search_meal_type',[\App\Http\Controllers\Admin_Panel\MealTypeController::class,'search']);
        Route::get('/reject_meal_type/{id}',[\App\Http\Controllers\Admin_Panel\MealTypeController::class,'reject']);
        Route::get('/accept_meal_type/{id}',[\App\Http\Controllers\Admin_Panel\MealTypeController::class,'accept']);

    });
});


Route::get('/token', function () {
    return csrf_token();
});

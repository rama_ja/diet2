@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"المستشارين</li>
                                    <li class="breadcrumb-item active"> عرض المستشار </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->

        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class=""> معلومات المستشار</h3>
                          </div>
                        <div class="" style="padding: 2%; font-weight: bold;">
                            <div class="container">

                                <div class="col-md-6 ">
                                    {{--                                        <label for="inputAddress" class="form-label "> الصورة الشخصية </label>--}}
                                    <img alt="avatar" src="/{{$advisor->personal_image}}"  width="200" height="200"/>
                                    <br><br>
                                </div>
                                <div class="col-md-6">
                                    <label  >الاسم : </label>
                                    <span>{{$advisor->name}}</span>
                                </div>
                                <div class="col-md-6">
                                    <label >رقم الجوال : </label>
                                    <span>{{$advisor->mobile}}</span>
                                </div>
                                <div class="col-md-6" >
                                     <label  > الاستشارة : </label>
                                    <span>{{$advisor->description}}</span>
                                </div>
                            </div>

                        </div>

                        </div>
                    </div>
                </div>
            </div>




{{--        <div class="container">--}}

{{--            <div class="col-md-6">--}}
{{--                <div class="col-md-6">--}}
{{--                    <label for="inputAddress" class="form-label"> الصورة الشخصية </label>--}}
{{--                    <img alt="avatar" src="/{{$advisor->personal_image}}"  width="200" height="200"/>--}}
{{--                    <br><br>--}}
{{--                </div>--}}


{{--                <div class="col-md-6">--}}
{{--                    <label for="inputEmail4" class="form-label">رقم الجوال</label>--}}
{{--                    <p>{{$advisor->mobile}}</p>--}}
{{--                </div>--}}

{{--                <div class="col-md-6">--}}
{{--                    <label for="inputEmail4" class="form-label">الاسم </label>--}}
{{--                    <p>{{$advisor->name}}</p>--}}
{{--                </div>--}}
{{--                <div class="col-md-6">--}}
{{--                    <label for="inputState" class="form-label"> الاستشارة </label>--}}
{{--                    <p>{{$advisor->description}}</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->
    </div>



@endsection

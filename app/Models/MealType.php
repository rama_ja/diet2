<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealType extends Model implements  TranslatableContract
{
    use HasFactory, Translatable;
    protected $fillable = ['status'];
    public $translatedAttributes = ['name'];

    public function meals()
    {
        return $this->hasMany(Meal::class);
    }

}
